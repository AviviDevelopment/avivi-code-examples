function(users) {
    var batch_size = pageSize * 50;
   var batchRes = [];
   for(let i in users)
   {
       let filter = {
           "USER_ID": users[i]['ID'],
           ">CREATED_DATE": firstDay,
           "<CREATED_DATE": lastDay
       };
       let pagesCount = parseInt(users[i]['TOTAL_RESULTS'] / pageSize) + 1;
       if(users[i]['TOTAL_RESULTS'] > 0)
       {
           if(users[i]['TOTAL_RESULTS'] > (batch_size + pageSize)) //учитываем первую страницу, которую получили раньше
           {
               var batches = 1;
               //считаем количество батчей чтоб получить все результаты
               if((users[i]['TOTAL_RESULTS'] % batch_size) != 0)
                   batches = Math.floor(users[i]['TOTAL_RESULTS'] / batch_size) + 1;
               else
                   batches = users[i]['TOTAL_RESULTS'] / batch_size;
               for(let j = 1; j < batches; j++)
               {
                   let minPage = (pageSize * j) - (pageSize - 2);
                   let maxPage = (pageSize * j) + 1;
                   if(maxPage > pagesCount)
                       maxPage = pagesCount;
                   batchRes.push(getBatch(minPage, maxPage, filter, pageSize));
               }
           }
           else if((users[i]['TOTAL_RESULTS'] > pageSize) && (users[i]['TOTAL_RESULTS'] <= (batch_size + pageSize)))
           {
               batchRes.push(getBatch(2, pagesCount, filter, pageSize));
           }
       }
   }

   return Promise.all(batchRes)
       .then(function(res){
           /*обработка результата*/
           return users;
       });
}

function getBatch(minPage, maxPage, filter, pageSize)
{
   let timeSubRequests = [];
   for(let page = minPage; page <= maxPage; page++)
   {
       timeSubRequests.push({
           method: "task.elapseditem.getlist",
           params: {
               order: {'ID': 'asc'},
               filter: filter,
               select: ["ID", "CREATED_DATE", "USER_ID", "MINUTES", "TASK_ID"],
               params: {
                   NAV_PARAMS: {
                       nPageSize: pageSize,
                       iNumPage: page
                   }
               }
           }
       });
   }
   return new Promise(function(resolve, reject){
       var returnRes = [];
       BX24.callBatch(
           timeSubRequests,
           function(batchSubResult)
           {
               for(let i in batchSubResult)
               {
                   returnRes.push(batchSubResult[i].data());
               }
               resolve(returnRes);
           }
       );
   });
}