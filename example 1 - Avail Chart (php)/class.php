<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

class AvailChartComponent extends CBitrixComponent
{
    /**
     * retured values
     * @var mixed
     */
	protected $returned;
	
	/**
	 * including lang files
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * prepare incoming parameters
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $result = array(
            "ELEMENT_ID" => trim($params["ELEMENT_ID"]),
            "IBLOCK_ID" => trim($params["IBLOCK_ID"])
        );
        return $result;
    }

	
	/**
	 * checking required modules
	 * @throws LoaderException
	 */
	protected function checkModules()
	{
		if (!Main\Loader::includeModule("crm") || !Main\Loader::includeModule("iblock") || !Main\Loader::includeModule("calendar"))
			throw new Main\LoaderException(Loc::getMessage("AVAIL_CHART_MODULE_NOT_INSTALLED"));
	}
	
	/**
	 * checking required parameters
	 * @throws SystemException
	 */
	protected function checkParams()
	{
		if ($this->arParams["ELEMENT_ID"] <= 0 || $this->arParams["ELEMENT_ID"] == "")
			throw new Main\ArgumentNullException("ELEMENT_ID");

		if ($this->arParams["IBLOCK_ID"] <= 0 || $this->arParams["IBLOCK_ID"] == "")
			throw new Main\ArgumentNullException("IBLOCK_ID");
	}
	

	/**
	 * fill static info
	 */
	protected function makeInfo()
	{
		$this->arResult = array(
			"ACTION" => (isset($_REQUEST["ACTION"])) ? $_REQUEST["ACTION"] : "",
			"IS_AJAX" => (isset($_REQUEST["IS_AJAX"]) && $_REQUEST["IS_AJAX"] == "Y") ? true : false,
			"MIN_DATE_START" => (isset($_REQUEST["DATA"]["MIN_DATE_START"])) ? strtotime($_REQUEST["DATA"]["MIN_DATE_START"]) : "", //get from POST or calculate later
			"DEPARTMENT_ID" => (isset($_REQUEST["DATA"]["DEPARTMENT_ID"])) ? $_REQUEST["DATA"]["DEPARTMENT_ID"] : "", //get from POST or calculate later
			"DEPARTMENTS" => array(),
			"USERS" => array(),
			"USERS_ID" => array(),
			"ELEMENT_INFO" => array(
				"ID" => $this->arParams["ELEMENT_ID"],
				"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
				"DATES_START" => array(),
				"RESPONSIBLE" => 0,
				"DEPARTMENT" => 0
			),
			"EVENTS" => array(),
			"ITEMS" => array(),
			"VIEW_SETTINGS" => array(
				"PERIOD" => array(),
				"PERIOD_LABELS" => array(),
				"PERIOD_START" => "",
				"PERIOD_START_FORMATTED" => "",
				"PERIOD_END" => "",
				"PERIOD_END_FORMATTED" => "",
				"CELL_SIZE" => ""
			),
			"WEEKENDS" => array(),
			"TODAY" => date("d.m.Y"),
			"YEAR" => date("Y"),
			"VIEW" => isset($_REQUEST["IS_AJAX"]) ? strtoupper($_REQUEST["DATA"]["VIEW"]) : "WEEKLY" //WEEKLY, DAILY, MONTHLY, or get from POST
		);
	}

	/**
	 * get result
	 */
	protected function getResult()
	{
    	//get departments
    	$this->getDepartments();

		//get element info
		if($this->arResult["ELEMENT_INFO"]["IBLOCK_ID"] == IB_AFTER_SALES)
		{
			$this->getASElementData();
		}
		else if($this->arResult["ELEMENT_INFO"]["IBLOCK_ID"] == IB_VIEWING_PLAN)
		{
			$this->getVPElementData();
		}

		if($this->arResult["ELEMENT_INFO"]["DATES_START"])
		{
			//get start date from element (if not set from POST)
			if(!$this->arResult["MIN_DATE_START"])
			{
				$this->arResult["MIN_DATE_START"] = min($this->arResult["ELEMENT_INFO"]["DATES_START"]);
			}

			//get aftersales responsible department
			if(!$this->arResult["DEPARTMENT_ID"])
			{
				$this->getASResponsibleDepartment();
			}

			//get aftersales responsible department users
			$this->getDepartmentUsers();

			//fill view settings
			$this->getViewSettings();

			if($this->arResult["USERS"])
			{
				//get user ovedues activities
				$this->getUsersOverdues();

				if($this->arResult["VIEW_SETTINGS"]["PERIOD"])
				{
					//get weekends
					$this->getWeekends();

					//get calendar events
					$this->getCalendarEvents();
					
					//get abcence events
					$this->getAbsenceEvents();
				}
			}

			// p($this->arResult["ITEMS"]);
			// p($this->arResult["USERS"]);
		}
	}

	/**
	 * get users overdues count
	 */
	protected function getUsersOverdues()
	{
		$arOverdue = array();
		$obActivity = CCrmActivity::GetList(
		    array(), 
		    array(
		        'CHECK_PERMISSIONS' => 'N', 
		        'RESPONSIBLE_ID' => $this->arResult["USERS_ID"],
		        '<DEADLINE' => CCalendar::Date(mktime()),
		        'COMPLETED' => 'N'
		    ), 
		    array('RESPONSIBLE_ID'),
		    false
		);
		while($arActivity = $obActivity->Fetch())
		{
			$this->arResult["USERS"][$arActivity["RESPONSIBLE_ID"]]["OVERDUES_COUNT"] = $arActivity["CNT"];
		}
	}

	/**
	 * get ABSENCE events
	 */
	protected function getAbsenceEvents()
	{
		$arAbsences = array();
		$arSelect = Array("ID", "NAME", "ACTIVE_FROM", "ACTIVE_TO", "PROPERTY_USER");
		$arFilter = Array(
			"IBLOCK_ID" => IB_ABSENCE_CHART,
			"ACTIVE"=>"Y",
			">=ACTIVE_FROM" => $this->arResult["VIEW_SETTINGS"]["PERIOD_START"],
			"<=ACTIVE_TO" => $this->arResult["VIEW_SETTINGS"]["PERIOD_END"],
			"PROPERTY_USER" => $this->arResult["USERS_ID"]
		);

		$obAbsences = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($arAbsence = $obAbsences->Fetch())
		{
			// HACK, make same keys as in calendar
			$arAbsence["DATE_FROM"] = $arAbsence["ACTIVE_FROM"];
			$arAbsence["DATE_TO"] = $arAbsence["ACTIVE_TO"];
			unset($arAbsence["ACTIVE_FROM"], $arAbsence["ACTIVE_TO"]);

			$this->arResult["EVENTS"]['ABSENCE'][$arAbsence["ID"]] = $arAbsence;
			$eventParams = array(
				'EVENT_DATE_FROM' => $arAbsence["DATE_FROM"],
				'EVENT_DATE_TO' => $arAbsence["DATE_TO"],
				'ITEM_ID' => $arAbsence["ID"],
				'OWNER_ID' => $arAbsence["PROPERTY_USER_VALUE"],
				'TYPE' => 'ABSENCE',
				"COLORS" => $this->getEventColors()
			);
			//build events structure
			$this->getViewBlocks($this->arResult["ITEMS"], $eventParams);
		}
	}

	/**
	 * get CALENDAR events
	 */
	protected function getCalendarEvents()
	{
		$arEventsIDs = array();
		$arEvents = CCalendarEvent::GetList(array(
			"userId" => 1,
			"getUserfields" => false,
			"parseRecursion" => true,
			"fetchAttendees" => true,
			"fetchMeetings" => true,
			"checkPermissions" => false,
			"preciseLimits" => true,
			"arFilter" => array(
				"OWNER_ID" => array_merge($this->arResult["USERS_ID"], array(0)),
				"FROM_LIMIT" => $this->arResult["VIEW_SETTINGS"]["PERIOD_START"],
				"TO_LIMIT" => $this->arResult["VIEW_SETTINGS"]["PERIOD_END"],
				"DELETED" => "N",
				"CAL_TYPE" => "company_calendar"
			)
		));
		foreach ($arEvents as $arEvent)
		{
			if($arEvent["OWNER_ID"] == 0)
			{
				if(in_array($arEvent["MEETING_HOST"] ,$this->arResult["USERS_ID"]))
				{
					$arEvent["OWNER_ID"] = $arEvent["MEETING_HOST"];
				}
				else
				{
					continue;
				}

			}

			$arEventsIDs[] = $arEvent["ID"];

			$arEvent["DETAIL_URL"] = "";
			$this->arResult["EVENTS"]["CALENDAR"][$arEvent["ID"]] = $arEvent;

			$eventParams = array(
				"EVENT_DATE_FROM" => $arEvent["DATE_FROM"],
				"EVENT_DATE_TO" => $arEvent["DATE_TO"],
				"ITEM_ID" => $arEvent["ID"],
				"OWNER_ID" => $arEvent["OWNER_ID"],
				"TYPE" => "CALENDAR",
				"COLORS" => $this->getEventColors()
			);

			$this->getViewBlocks($this->arResult["ITEMS"], $eventParams);
		}
		if (!empty($arEventsIDs))
		{
			$this->getEventsDetailUrl($arEventsIDs);
		}
	}

	/**
	 * get calendar events bind elemets urls (After Sales and Viewing Plan elements)
	 */
	protected function getEventsDetailUrl($arEventsIDs)
	{
		$elementDetailUrl = '/services/lists/#list_id#/element/0/#element_id#/';
		$arBinds = CHighData::GetList(HB_CAL_EV_TO_LIST_BIND, array("UF_EVENT_ID" => $arEventsIDs));
		foreach ($arBinds as $arBind) 
		{
		    $this->arResult["EVENTS"]['CALENDAR'][$arBind["UF_EVENT_ID"]]["DETAIL_URL"] = str_replace(
		        array("#list_id#", "#element_id#"),
		        array($arBind["UF_IBLOCK_ID"], $arBind["UF_ELEMENT_ID"]), 
		        $elementDetailUrl
		    );
		}
	}

	/**
	 * get after sales element data
	 */
	protected function getASElementData()
	{
		$arElementFilter = array("ID" => $this->arParams["ELEMENT_ID"], "IBLOCK_ID" => $this->arResult["ELEMENT_INFO"]["IBLOCK_ID"]);
		$dbElements = CIBlockElement::GetList(
			array(), $arElementFilter, false, false, 
			array("ID", "PROPERTY_MEETING_START_DATE", "PROPERTY_AFTER_SALES_RESPONSIBLE")
		);
		while($arElement = $dbElements->Fetch())
		{
			$this->arResult["ELEMENT_INFO"]["DATES_START"][] = strtotime($arElement["PROPERTY_MEETING_START_DATE_VALUE"]);
			$this->arResult["ELEMENT_INFO"]["RESPONSIBLE"] = $arElement["PROPERTY_AFTER_SALES_RESPONSIBLE_VALUE"];
		}
	}

	/**
	 * get viewing plan element data
	 */
	protected function getVPElementData()
	{
		$arElementFilter = array("ID" => $this->arParams["ELEMENT_ID"], "IBLOCK_ID" => $this->arResult["ELEMENT_INFO"]["IBLOCK_ID"]);
		$dbElements = CIBlockElement::GetList(
			array(), $arElementFilter, false, false, 
			array("ID", "PROPERTY_MEETING_DATE", "PROPERTY_SELECT_SELLER")
		);
		if($arElement = $dbElements->Fetch())
		{
			$this->arResult["ELEMENT_INFO"]["DATES_START"][] = strtotime($arElement["PROPERTY_MEETING_DATE_VALUE"]);
			$this->arResult["ELEMENT_INFO"]["RESPONSIBLE"] = $arElement["PROPERTY_SELECT_SELLER_VALUE"];
		}
	}

	/**
	 * get after sales responsible department
	 */
	protected function getASResponsibleDepartment()
	{
		$dbASUser = CUser::GetList(
			($by = "ID"), ($order = "desc"),
			array("ID" => $this->arResult["ELEMENT_INFO"]["RESPONSIBLE"]),
			array("SELECT" => array("UF_DEPARTMENT"), "FIELDS" => array("ID"))
		);
		if ($arASUser = $dbASUser->Fetch()) 
		{    
			if($arASUser["UF_DEPARTMENT"])
			{
				$this->arResult["DEPARTMENT_ID"] = array_shift($arASUser["UF_DEPARTMENT"]);
			}
		}
	}

	/**
	 * get department Users
	 */
	protected function getDepartmentUsers()
	{
		$arDeptUsers = array();
		$rsDeptUsers = CUser::GetList(
			($by = "NAME"), ($order = "ASC"),
			array("UF_DEPARTMENT" => $this->arResult["DEPARTMENT_ID"], "GROUPS_ID" => array(AGENTS_GROUP_ID)),
			array("FIELDS" => array("ID", "NAME", "LAST_NAME", "PERSONAL_PHOTO"))
		);
		while ($arDeptUser = $rsDeptUsers->Fetch()) 
		{   
			if($arDeptUser["PERSONAL_PHOTO"])
			{
				$arFile = CFile::ResizeImageGet(
					$arDeptUser["PERSONAL_PHOTO"],
					array("width" => 44, "height" => 44),
					BX_RESIZE_IMAGE_EXACT
				);
				$arDeptUser["PERSONAL_PHOTO"] = $arFile["src"];
			}
			
			$arDeptUser["FULL_NAME"] = $arDeptUser["NAME"]." ".$arDeptUser["LAST_NAME"];
			$arDeptUser["OVERDUES_COUNT"] = 0;

			$this->arResult["USERS"][$arDeptUser["ID"]] = $arDeptUser;
			$this->arResult["USERS_ID"][] = $arDeptUser["ID"];
		}
		unset($arDeptUsers, $rsDeptUsers);
	}

	/**
	 * make view period
	 */
	protected function getViewSettings()
	{
		//get month label 
		$this->arResult["VIEW_SETTINGS"]["MONTH_LABEL"] = date("M", $this->arResult["MIN_DATE_START"]);

		switch($this->arResult["VIEW"])
		{
			case "WEEKLY":
				//get week number 
				$this->arResult["VIEW_SETTINGS"]["WEEK_NUMBER"] = date("W", $this->arResult["MIN_DATE_START"]);

				//set before noon paading
				$this->arResult["VIEW_SETTINGS"]["VIEW_TYPE"]["BEFORE"] = 0;
				//set after noon paading
				$this->arResult["VIEW_SETTINGS"]["VIEW_TYPE"]["AFTER"] = 27;
				//set cell size
				$this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] = 200;

				//get period					
				$periodStart = new \DateTime(date("d.m.Y", $this->arResult["MIN_DATE_START"]));

				//get previous/next period dates
				$this->arResult["VIEW_SETTINGS"]["PREV_PERIOD"] = date("d.m.Y", strtotime("-1 week", $this->arResult["MIN_DATE_START"]));
				$this->arResult["VIEW_SETTINGS"]["NEXT_PERIOD"] = date("d.m.Y", strtotime("+1 week", $this->arResult["MIN_DATE_START"]));

				$periodDates = new DatePeriod(
					clone $periodStart->modify(("Sunday" == $periodStart->format("l")) ? "Monday last week" : "Monday this week"),
					new DateInterval("P1D"),
					clone $periodStart->modify("Monday next week")
				);

				//fill start/end of period
				$this->arResult["VIEW_SETTINGS"]["PERIOD_START"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->start->format("d.m.Y"), false),
					true, false
				);
				$this->arResult["VIEW_SETTINGS"]["PERIOD_START_FORMATTED"] = $periodDates->start->format("d.m.Y");

				$this->arResult["VIEW_SETTINGS"]["PERIOD_END"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->end->modify('-1 day')->format("d.m.Y 23:59"), false),
					true, false
				); // -1 day, bacause of Monday next week
				$this->arResult["VIEW_SETTINGS"]["PERIOD_END_FORMATTED"] = $periodDates->end->modify('-1 day')->format("d.m.Y");

				//set formatted period
				$this->arResult["PERIOD_FORMATTED"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_START_FORMATTED"]." - ".$this->arResult["VIEW_SETTINGS"]["PERIOD_END_FORMATTED"];

				//fill period
				foreach ($periodDates as $periodDate) {
					$dateWOLabel = $periodDate->format("d.m.Y");
					$dateWithLabel = $periodDate->format("l d");
					$dateWOYear = $periodDate->format("d.m");

					$this->arResult["VIEW_SETTINGS"]["PERIOD"][] = $dateWOLabel;
					$this->arResult["VIEW_SETTINGS"]["PERIOD_LABELS"][$dateWOLabel]["LABEL"] = $dateWithLabel;
					$this->arResult["VIEW_SETTINGS"]["PERIOD_LABELS"][$dateWOLabel]["DATE"] = $dateWOYear;
				}
				break;
			case "MONTHLY":
				//set before noon paading
				$this->arResult["VIEW_SETTINGS"]["VIEW_TYPE"]["BEFORE"] = 0;
				//set after noon paading
				$this->arResult["VIEW_SETTINGS"]["VIEW_TYPE"]["AFTER"] = 27;
				//set cell size
				$this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] = 54;

				//get period	
				$sourceDate = new DateTime(date("d.m.Y", $this->arResult["MIN_DATE_START"]));

				//get previous/next period dates
				$this->arResult["VIEW_SETTINGS"]["PREV_PERIOD"] = date("d.m.Y", strtotime("-1 month", $this->arResult["MIN_DATE_START"]));
				$this->arResult["VIEW_SETTINGS"]["NEXT_PERIOD"] = date("d.m.Y", strtotime("+1 month", $this->arResult["MIN_DATE_START"]));

				$periodDates = new DatePeriod(
				    clone $sourceDate->modify('first day of'),
				    new DateInterval('P1D'),
				    clone $sourceDate->modify('last day of')->modify('+ 1 day')
				);

				//fill start/end of period
				$this->arResult["VIEW_SETTINGS"]["PERIOD_START"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->start->format("d.m.Y"), false),
					true, false
				);
				$this->arResult["VIEW_SETTINGS"]["PERIOD_START_FORMATTED"] = $periodDates->start->format("d.m.Y");

				$this->arResult["VIEW_SETTINGS"]["PERIOD_END"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->end->modify('-1 day')->format("d.m.Y 23:59"), false),
					true, false
				); // -1 day, bacause of +1 day to period end
				$this->arResult["VIEW_SETTINGS"]["PERIOD_END_FORMATTED"] = $periodDates->end->modify('-1 day')->format("d.m.Y");
				//set formatted period
				$this->arResult["PERIOD_FORMATTED"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_START_FORMATTED"]." - ".$this->arResult["VIEW_SETTINGS"]["PERIOD_END_FORMATTED"];

				//fill period
				foreach ($periodDates as $periodDate) 
				{
				    $dateWOLabel = $periodDate->format("d.m.Y");
					$dateWithLabel = $periodDate->format("D j");
					$dateWOYear = $periodDate->format("d.m");
					$dateNum = $periodDate->format("N");
					$weekNum = $periodDate->format("W");

					$this->arResult["VIEW_SETTINGS"]["PERIOD"][] = $dateWOLabel;
					$this->arResult["VIEW_SETTINGS"]["PERIOD_LABELS"][$dateWOLabel] = array(
						"LABEL" => $dateWithLabel,
						"DATE" => $dateWOYear,
						"DAY_NUM" => $dateNum,
						"WEEK_NUM" => $weekNum
					);
				}
				break;
			case "DAILY":
				//set cell size
				$this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] = 1296;
				//get period	
				$sourceDate = new DateTime(date("d.m.Y", $this->arResult["MIN_DATE_START"]));

				//get previous/next period dates
				$this->arResult["VIEW_SETTINGS"]["PREV_PERIOD"] = date("d.m.Y", strtotime("-1 day", $this->arResult["MIN_DATE_START"]));
				$this->arResult["VIEW_SETTINGS"]["NEXT_PERIOD"] = date("d.m.Y", strtotime("+1 day", $this->arResult["MIN_DATE_START"]));

				$periodDates = new DatePeriod(
			        clone $sourceDate,
			        new DateInterval('PT1H'),
			        clone $sourceDate->modify('+ 1 day')
			    );

				//fill start/end of period
				$this->arResult["VIEW_SETTINGS"]["PERIOD_START"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->start->format("d.m.Y"), false),
					true, false
				);
				$this->arResult["VIEW_SETTINGS"]["PERIOD_END"] = CCalendar::Date(
					CCalendar::Timestamp($periodDates->end->modify('-1 day')->format("d.m.Y 23:59"), false),
					true, false
				); // -1 day, bacause of +1 day to period end

				//set formatted period
				$this->arResult["PERIOD_FORMATTED"] = date("d.m.Y", $this->arResult["MIN_DATE_START"]);

				$this->arResult["VIEW_SETTINGS"]["PERIOD"][] = $this->arResult["PERIOD_FORMATTED"];
			    foreach ($periodDates as $periodDate) 
			    {
					$this->arResult["VIEW_SETTINGS"]["PERIOD_LABELS"][] = array(
						"LABEL" => $periodDate->format('H:i')
					);
			    }
			 //    $this->arResult["VIEW_SETTINGS"]["PERIOD_LABELS"][] = array(
				// 	"LABEL" => "23:59"
				// );
				break;
		}
	}

	/**
	 * get departments
	 */
	protected function getDepartments()
	{
		$arDeptFilter = array("IBLOCK_ID" => IB_DEPARTMENTS, "ACTIVE" => "Y", "!ID" => EXLUDED_DEPARTMENTS);
		$dbDepts = CIBlockSection::GetList(array("NAME" => "ASC"), $arDeptFilter, false, array("ID", "NAME"));
		while ($arDept = $dbDepts->Fetch())
		{
		    $this->arResult["DEPARTMENTS"][] = $arDept;
		}
	}

	/**
	 * get time in hours and minutes from seconds
	 */
	protected function timeLength($sec = 0, $split = false)
	{
		$s = $sec % 60;
		$m = (($sec-$s) / 60) % 60;
		$h = floor($sec / 3600);
		
		$h = substr("0".$h,-2);
		$m = substr("0".$m,-2);
		
		if($h == "") $h = "00";
		if($m == "") $m = "00";
		
		if($split) return array("H" => $h, "M" => $m);
		else return $h.":".$m;
	}


	/**
	 * get weekends
	 */
	protected function getWeekends()
	{
		$arWeekends = explode(",", COption::GetOptionString("calendar", "year_holidays"));
		foreach($arWeekends as $weekend)
		{
			$this->arResult["WEEKENDS"][] = date("d.m", strtotime($weekend.".".$this->arResult["YEAR"]));
		}
	}

	/**
	 * get view blocks from event data
	 */
	protected function getViewBlocks(&$arItems, $params)
	{	
		//fix end date in case H:i == 00:00
		if(date("H:i", strtotime($params["EVENT_DATE_TO"])) == "00:00")
		{
			$params["EVENT_DATE_TO"] = date("d.m.Y 23:59", strtotime($params["EVENT_DATE_TO"]));
		}

		//fill chart event info
		$arEvent = array(
			"TYPE" => $params["TYPE"],
			"EVENT_ID" => $params["ITEM_ID"],
			"COLORS" => $params["COLORS"],
			"TIME_LENGTH" => $this->timeLength(
				strtotime($params["EVENT_DATE_TO"]) - strtotime($params["EVENT_DATE_FROM"])
			) //get event time length
		);

		//monthly and weekly view
		if($this->arResult["VIEW"] == "WEEKLY" || $this->arResult["VIEW"] == "MONTHLY")
		{
			$viewBeforeNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"]/12600;
			$viewAfterNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"]/18000;

			$startBeforeNoon = strtotime("8:30");
			$endBeforeNoon = strtotime("12:00");
			$startAfterNoon = strtotime("13:00");
			$endAfterNoon = strtotime("18:00");


			//event start date before period start date
			if (strtotime($params["EVENT_DATE_FROM"]) < strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_START"])) 
			{
				$params["EVENT_DATE_FROM"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_START"];
			}
			//event end date after period end date
			if (strtotime($params["EVENT_DATE_TO"]) > strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_END"])) 
			{
				$params["EVENT_DATE_TO"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_END"];
			}

			$startTimeStr = date("H:i", strtotime($params["EVENT_DATE_FROM"]));
			$endTimeStr = date("H:i", strtotime($params["EVENT_DATE_TO"]));

			$startEventTime = strtotime($startTimeStr);
			$endEventTime = strtotime($endTimeStr);

			// HACK
			if ($endTimeStr == "00:00")
			{
				$endEventTime = strtotime("23:59");
				// $params["EVENT_DATE_TO"] = date("d.m.Y 23:59", strtotime($params["EVENT_DATE_TO"]));
			}
			if ($startEventTime >= $endBeforeNoon && $startEventTime <= $startAfterNoon)
			{
				$startEventTime = strtotime("13:00");
			}
			if ($endEventTime > $endBeforeNoon && $endEventTime < $startAfterNoon) 
			{
				$endEventTime = strtotime("12:00");
			}
			// END HACK

			$periodBegin = new DateTime(date("d.m.Y", strtotime($params["EVENT_DATE_FROM"])));
			$periodEnd = new DateTime(date("d.m.Y", strtotime($params["EVENT_DATE_TO"])));
			$periodEnd = $periodEnd->modify("+1 day"); 

			// $obPeriod = new DatePeriod(
			//     new \DateTime($params["EVENT_DATE_FROM"]),
			//     new DateInterval("P1D"),
			//     new \DateTime($params["EVENT_DATE_TO"])
			// );
			$arPeriod = array();
			$obPeriod = new DatePeriod($periodBegin, new DateInterval("P1D"), $periodEnd);
			foreach ($obPeriod as $period) 
			{
				$arPeriod[] = $period->format("d.m.Y");
			}

			foreach ($arPeriod as $key => $eventDate)
			{
				$startTime = strtotime("8:30");
				$endTime = strtotime("18:00");
				if ($key == 0) 
				{
					$startTime = $startEventTime;
				}

				if (!isset($arPeriod[$key+1])) 
				{
					$endTime = $endEventTime;
				}
				if ($startTime > $endTime)
				{
					continue;
				}

				$shiftBeforeNoon = 0;
				$sizeBeforeNoon = 0;

				$shiftAfterNoon = 0;
				$sizeAfterNoon = 0;

				// start time befor/equal 8:30
				if ($startTime <= $startBeforeNoon) 
				{
					$shiftBeforeNoon = 0;
					// end time before 12:00
					if ($endTime <= $endBeforeNoon) 
					{
						$sizeBeforeNoon = round($this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] - (($endBeforeNoon - $endTime) * $viewBeforeNoon) - $shiftBeforeNoon);
					}
					elseif ($endTime >= $startAfterNoon) // end time after 13:00
					{
						$sizeBeforeNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"];
						$shiftAfterNoon = 0;

						// end time after/equal 18:00
						if ($endTime >= $endAfterNoon) 
						{
							$sizeAfterNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"];
						}
						else // end time before 18:00
						{
							$sizeAfterNoon = round(($endTime - $startAfterNoon) * $viewAfterNoon);
						}
					}
				}
				// start time after 8:30
				elseif ($startTime > $startBeforeNoon)
				{
					// at which half the start time is
					// before 12:00
					if ($startTime <= $endBeforeNoon)
					{
						$shiftBeforeNoon = round(($startTime - $startBeforeNoon) * $viewBeforeNoon);
						
						// end time before 12:00
						if ($endTime <= $endBeforeNoon) 
						{
							$sizeBeforeNoon = round($this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] - (($endBeforeNoon - $endTime) * $viewBeforeNoon) - $shiftBeforeNoon);
						}
						elseif ($endTime >= $startAfterNoon) // end time after 13:00
						{
							$sizeBeforeNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] - $shiftBeforeNoon;
							$shiftAfterNoon = 0;

							// end time after/equal 18:00
							if ($endTime >= $endAfterNoon) 
							{
								$sizeAfterNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"];
							}
							else // end time before 18:00
							{
								$sizeAfterNoon = round(($endTime - $startAfterNoon) * $viewAfterNoon);
							}

						}
					}
					elseif($startTime >= $startAfterNoon) // start time after 13:00
					{
						$shiftAfterNoon = round(($startTime - $startAfterNoon) * $viewAfterNoon);

						// end time after/equal 18:00
						if ($endTime >= $endAfterNoon) 
						{
							$sizeAfterNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] - $shiftAfterNoon;
						}
						else // end time before 18:00
						{
							$sizeAfterNoon = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"] - $shiftAfterNoon - round(($endAfterNoon - $endTime) * $viewAfterNoon);
						}
					}
				}
				if ($sizeBeforeNoon != 0 || $sizeAfterNoon != 0) 
				{
					if (!isset($arItems[$params["OWNER_ID"]])) 
					{
						$arItems[$params["OWNER_ID"]] = array();
					}

					// set default before/after empty arrays
					if (!isset($arItems[$params["OWNER_ID"]][$eventDate])) 
					{
						$arItems[$params["OWNER_ID"]][$eventDate] = array();
					}

					//fill before noon events array
					if ($sizeBeforeNoon != 0) 
					{
						$arItems[$params["OWNER_ID"]][$eventDate][] = array_merge($arEvent, array(
							"SHIFT" => $shiftBeforeNoon,
							"SIZE" => $sizeBeforeNoon,
							"NOON" => "BEFORE"
						));
					}

					//fill after noon events array
					if ($sizeAfterNoon != 0) 
					{
						$arItems[$params["OWNER_ID"]][$eventDate][] = array_merge($arEvent, array(
							"SHIFT" => $shiftAfterNoon,
							"SIZE" => $sizeAfterNoon,
							"NOON" => "AFTER"
						));
					}
				}
			}
		}
		else if($this->arResult["VIEW"] == "DAILY")
		{
			if(strtotime($params["EVENT_DATE_FROM"]) > strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_END"]))
			{
				return;
			}

    		// $secondSize = $this->arResult["VIEW_SETTINGS"]["CELL_SIZE"]/86340;
    		$secondSize = 0.015; //1296 / 54 / 60 / 60
    		$eventShift = 0;
    		$eventSize = 0;

			//event start date before period start date
			if (strtotime($params["EVENT_DATE_FROM"]) < strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_START"])) 
			{
				$params["EVENT_DATE_FROM"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_START"];
			}
			//event end date after period end date
			if (strtotime($params["EVENT_DATE_TO"]) > strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_END"])) 
			{
				$params["EVENT_DATE_TO"] = $this->arResult["VIEW_SETTINGS"]["PERIOD_END"];
			}

			$eventShift = round((strtotime($params["EVENT_DATE_FROM"]) - strtotime($this->arResult["VIEW_SETTINGS"]["PERIOD_START"])) * $secondSize);
    		$eventSize = round((strtotime($params["EVENT_DATE_TO"]) - strtotime($params["EVENT_DATE_FROM"])) * $secondSize);

    		//hack, add 1 px in case en time is 23:59 
    		if(date("H:i", strtotime($params["EVENT_DATE_TO"])) == "23:59")
    		{
    			$eventSize++;
    		}

    		if($eventSize != 0)
    		{
	    		$arItems[$params["OWNER_ID"]][$this->arResult["PERIOD_FORMATTED"]][] = array_merge($arEvent, array(
					"SHIFT" => $eventShift,
					"SIZE" => $eventSize
				));
    		}
		}
	}

	/**
	 * get event and font colors
	 */
	protected function getEventColors()
	{
		$tone = rand(0,1);

		$arColors = array(
	        array( '#F2D7D5', '#922B21' ),
	        array( '#FADBD8', '#B03A2E' ),
	        array( '#EBDEF0', '#76448A' ),
	        array( '#E8DAEF', '#6C3483' ),
	        array( '#D4E6F1', '#1F618D' ),
	        array( '#AED6F1', '#2874A6' ),
	        array( '#D1F2EB', '#148F77' ),
	        array( '#A2D9CE', '#117A65' ),
	        array( '#7DCEA0', '#1E8449' ),
	        array( '#D5F5E3', '#239B56' ),
	        array( '#FCF3CF', '#B7950B' ),
	        array( '#FAD7A0', '#B9770E' ),
	        array( '#F8C471', '#9C640C' ),
	        array( '#F5CBA7', '#AF601A' ),
	        array( '#EF5350', '#B71C1C' ),
	        array( '#F48FB1', '#C2185B' ),
	        array( '#CE93D8', '#6A1B9A' ),
	        array( '#7986CB', '#1A237E' ),
	        array( '#64B5F6', '#1565C0' ),
	        array( '#00BCD4', '#006064' ),
	    );

		return array(
			"COLOR" => $arColors[array_rand($arColors)][$tone],
			"FONT_COLOR" => $tone > 0 ? '#fff' : '#000'
		);
	}	

	public static function returnJson($data)
	{
		global $APPLICATION;

		$APPLICATION->restartBuffer();

		header('Content-Type: application/x-javascript; charset=UTF-8');
		echo \Bitrix\Main\Web\Json::encode($data);
		die();
	}

	/**
	 * component logic
	 */
	public function executeComponent()
	{
		try
		{
			$this->checkModules();
			$this->checkParams();
			$this->makeInfo();
			$this->getResult();
			$this->includeComponentTemplate();

			// return $this->returned;
		}
		catch (Exception $e)
		{
			ShowError($e->getMessage());
		}
	}
}
?>