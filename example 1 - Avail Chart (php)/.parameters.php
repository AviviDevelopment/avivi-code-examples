<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'ELEMENT_ID' =>  array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AVAIL_CHART_ELEMENT_ID'),
				'TYPE' => 'STRING',
				'DEFAULT' => '0'
			),
			'IBLOCK_ID' =>  array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AVAIL_CHART_IBLOCK_ID'),
				'TYPE' => 'STRING',
				'DEFAULT' => '0'
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>