<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('AVAIL_CHART_DESCRIPTION_NAME'),
	"DESCRIPTION" => Loc::getMessage('AVAIL_CHART_DESCRIPTION_DESCRIPTION'),
	"SORT" => 20,
	"PATH" => array(
		"ID" => 'avivi',
		"NAME" => Loc::getMessage('AVAIL_CHART_DESCRIPTION_GROUP'),
		"SORT" => 10
	),
);

?>