<? 
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("HLBLOCK_ID", 1); // Highload block ID
define("IMAP_USERID", '*********'); // IMAP user ID
define("IMAP_PASS", '********'); // IMAP user password
define("DEF_TASK_MANAGER", 1); // default task manager
define("GROUPS_ID", 13); // default task manager
define("SUPPORT_GROUP_ID", 22); // support group id
ini_set('max_execution_time', 1800);
ini_set('memory_limit', '2000M');
set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//To remove all the hidden text not displayed on a webpage
function strip_html_tags($str){
    $str = preg_replace('/(<|>)\1{2}/is', '', $str);
    $str = preg_replace(
        array(// Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
        ),
        "", //replace above with nothing
        $str );
    $str = replaceWhitespace($str);
    $str = strip_tags($str);
    return $str;
} //function strip_html_tags ENDS

//To replace all types of whitespace with a single space
function replaceWhitespace($str) {
    $result = $str;
    foreach (array(
    "  ", " \t",  " \r",  " \n",
    "\t\t", "\t ", "\t\r", "\t\n",
    "\r\r", "\r ", "\r\t", "\r\n",
    "\n\n", "\n ", "\n\t", "\n\r",
    ) as $replacement) {
    	$result = str_replace($replacement, $replacement[0], $result);
    }
    return $str !== $result ? replaceWhitespace($result) : $result;
} //function replaceWhitespace ENDS

// connect required modules
if(!CModule::IncludeModule('crm')) exit("crm module not found");
if(!CModule::IncludeModule('socialnetwork')) exit("socialnetwork module not found");
if(!CModule::IncludeModule("tasks")) exit("tasks module not found");
if(!CModule::IncludeModule('highloadblock')) exit("highloadblock module not found");

// Open the IMAP stream to the mailbox
$inbox = imap_open('{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX', IMAP_USERID, IMAP_PASS) or die('Cannot connect to Gmail: ' . imap_last_error());

// We receive messages that meet the specified criteria (today's date)
$arEmails = imap_search($inbox,'SINCE "'.date('j F Y').'"'); 

// Preparing an array for creating tasks START
if($arEmails) 
{
	// put the newest emails on top 
	rsort($arEmails);
	// Participants in group
	$arParticipants = CGroup::GetGroupUser(SUPPORT_GROUP_ID);
	// uid in Box
	$arUidsInBox = array();
	// uid in Bitrix
	$arUidsInBitrix = array();
	// for every email... 
	foreach($arEmails as $email_number) 
	{
		// Overview of information contained in message headers
		$overview = imap_fetch_overview($inbox,$email_number,0);
		$arUidsInBox[] = $overview[0]->uid;
	}

	// Create an instance of the entity HL block
	$obHLblock = Bitrix\Highloadblock\HighloadBlockTable::getById(HLBLOCK_ID)->fetch(); 
	$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($obHLblock); 
	$entity_data_class = $entity->getDataClass(); 
	$entity_table_name = $obHLblock['TABLE_NAME']; 
	$sTableID = 'tbl_'.$entity_table_name;

	// We make a selection of elements by UID letters
	$obHLlist = $entity_data_class::getList(array("select" => array('UF_UID'), "filter" => array('UF_UID'=>$arUidsInBox),"order" => array("UF_UID"=>"ASC") ) );
	$obHLlist = new CDBResult($obHLlist, $sTableID);
	while($arHLlist = $obHLlist->Fetch()){
		$arUidsInBitrix[] = $arHLlist['UF_UID'];	
	}
	
	// UID letters for processing
	$arUids = array_diff($arUidsInBox, $arUidsInBitrix);

	// for every email... 
	if(!empty($arUids)) 
	{
		foreach($arEmails as $email_number) 
		{
			// Overview of information contained in message headers
			$overview = imap_fetch_overview($inbox,$email_number,0);
			// Check if the message was being processed
			if (in_array($overview[0]->uid, $arUids)) 
			{
				$email = '';
				$user_id = '';
				$company_id = '';
				$contact_id = '';
				$responsible_id = 1;
				$group_id = '';
				$file_url = array();

				// The headline of the message 
				$msg_header = imap_headerinfo($inbox, $email_number);
				// We get the message itself
				$message = imap_fetchbody($inbox,$email_number,1.1) ? imap_fetchbody($inbox,$email_number,1.1) : imap_fetchbody($inbox,$email_number,1); 
				$message = replaceWhitespace(strip_html_tags($message)); // Task description
				// Structure of the message
				$structure = imap_fetchstructure($inbox,$email_number); 

				// Save attachments from the message
				foreach ($structure->parts as $key => $value) 
				{
					if($value->disposition == 'ATTACHMENT')
					{
						// File name
						$file_name = imap_utf8($value->dparameters[0]->value);			   
						// File
						$attach = imap_fetchbody($inbox, $email_number, $key+1);		
						$attach = base64_decode($attach);
						file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tasks/attach/'.$file_name, $attach);
						$file_url[] = $_SERVER['DOCUMENT_ROOT'].'/tasks/attach/'.$file_name; // files url
					}
				}
				
				// Sender's email
				$email = $msg_header->from[0]->mailbox.'@'.$msg_header->from[0]->host;
				if($email)
				{
					// contact and company values
					$arValuesField = array();
					$arFilterEmail = array("TYPE_ID" => "EMAIL", "VALUE_TYPE" => "WORK", "VALUE" => $email);
					$obFields = CCrmFieldMulti::GetList(array(), $arFilterEmail);
					while($arField = $obFields->Fetch()){
						if ($arField['ENTITY_ID'] == 'COMPANY') 
						{
							$arValuesField['COMPANY'] = $arField;
							$company_id = $arField['ELEMENT_ID']; //  Company of the task
						}
						elseif ($arField['ENTITY_ID'] == 'CONTACT') 
						{
							$arValuesField['CONTACT'] = $arField;
							$contact_id = $arField['ELEMENT_ID']; //  Contact of the task
						}
					}	
					// Search and link the company to the task
					if ($company_id > 0) 
					{					
						$obCompany = CCrmCompany::GetListEx(array('TITLE'=>'ASC'), array("NAME", "CHECK_PERMISSIONS" => "N", "ID" => $company_id), false, false, array());
						$arCompany = $obCompany->Fetch();				
						$responsible_id = $arCompany['ASSIGNED_BY_ID']; // Responsible of the task
					}

					// Search and link the task manager to the task
					$userParams = array('FIELDS' => array('EMAIL','ID'));
					$arUserFilter = array("ACTIVE" => "Y", "EMAIL" => $email, "GROUPS_ID" => array(GROUPS_ID));
					$obUsers = CUser::GetList($by="id", $order="asc", $arUserFilter, $userParams); 
					$arUser = $obUsers->Fetch();
					if($arUser)
					{
						$user_id = $arUser['ID']; // task manager
					}
					else
					{
						$user_id = DEF_TASK_MANAGER; // task manager
						$arUserDefFilter = array("ACTIVE" => "Y", "ID" => DEF_TASK_MANAGER);
						
						$obUserAdmin = CUser::GetList($by="id", $order="asc", $arUserDefFilter, $userParams);
						$arUserAdmin = $obUserAdmin->Fetch();
						$email = $arUserAdmin['EMAIL']; // task manager EMAIL
					}

					// Search and link the workgroup to the task
					$obRelations = CSocNetUserToGroup::GetList(array(), array("ROLE" => SONET_ROLES_USER, "USER_ACTIVE" => "Y", "USER_EMAIL" => $email), false, false, array("USER_EMAIL", "USER_ID", "GROUP_NAME", "GROUP_ID"));
					$arRelations = $obRelations->GetNext();
					$group_id = $arRelations['GROUP_ID'];
				}

				// create an array of fields and write to the variable arTasks
				$arTasks[] = array(
					"TITLE" => $email.' - '.$overview[0]->subject, 
					"DESCRIPTION" => $message, 
					"COMPANY_ID" => $company_id, 
					"CONTACT_ID" => $contact_id, 
					"RESPONSIBLE_ID" => $responsible_id, 
					"ACCOMPLICES" => $arParticipants,
					"CREATED_BY" => $user_id, 
					"CREATED_BY_EMAIL" => $email, 
					"GROUP_ID" => $group_id, 
					"TAGS" => "Support",
					"ID_MESSAGE" => $overview[0]->uid,
					"FILE" => $file_url
				);
			}
		}
	}
	else
	{
		echo "All messages processed";
	}
}
else
{
	echo "No messages for today";
}
// Preparing an array for creating tasks END

// close the connection 
imap_close($inbox);

// create tasks and attach files to them START
$arUnlinkFiles = array();
if(!empty($arTasks)) 
{
	foreach ($arTasks as $key => $arTask) 
	{
		// creating an instance of a new entity
		$task = new \Bitrix\Tasks\Item\Task(0, $arTask['CREATED_BY']);
		// An array of data fields for the task
		$task['TITLE'] = $arTask['TITLE'];
		$task['DESCRIPTION'] = $arTask['DESCRIPTION'];
		$task['RESPONSIBLE_ID'] = $arTask['RESPONSIBLE_ID'];
		$task['ACCOMPLICES'] = $arTask['ACCOMPLICES'];
		$task['CREATED_BY'] = $arTask['CREATED_BY'];
		$task['GROUP_ID'] = $arTask['GROUP_ID'];
		$task['TAGS'] = $arTask['TAGS'];
		$task['UF_CRM_TASK'] = array('CO_'.$arTask['COMPANY_ID'], 'C_'.$arTask['CONTACT_ID']);		

		// Create task
		$result = $task->save();

		if($result->isSuccess())
		{
			echo "OK";
			// get the ID of the created task
		    $refInstance = new \ReflectionClass($result);
		    $propInstance = $refInstance->getProperty('instance');
		    $propInstance->setAccessible(true);
		    $instance = $propInstance->getValue($result);

		    $refId = new \ReflectionClass($instance);
		    $propId = $refId->getProperty('id');
		    $propId->setAccessible(true);
		    $taskId = $propId->getValue($instance);
			

		    if (!empty($arTask['FILE']))
			{                
	            // Define a class for working with Bitrix disk
	            $USER_ID = $arTask['CREATED_BY'];
	            $storage = Bitrix\Disk\Driver::getInstance()->getStorageByUserId($USER_ID);
	            $folder = $storage->getFolderForUploadedFiles();
	            
	            // Array with files
	            $newFiles = array();
	            
	            // copy files to a Bitrix disk
	            foreach($arTask['FILE'] as $file_url){
	                $arFile = CFile::MakeFileArray($file_url);
	                $file = $folder->uploadFile($arFile, array(
	                   'NAME' => $arFile["name"],
	                   'CREATED_BY' => $USER_ID
	                ), array(), true);
	                
	                // Adding id files to an array with the prefix n
	                $newFiles[] = "n".$file->getId();
	                if (!in_array($val, $arUnlinkFiles))
	                	$arUnlinkFiles[] = $val;
	            }

	            // Attach files to the task
	            if(!empty($newFiles)){
	                $obTaskItem = new CTaskItem($taskId, $USER_ID);
	                $resAddFiles = $obTaskItem->Update(array("UF_TASK_WEBDAV_FILES" => $newFiles));               
	            }		    
			}
			// We add the UID of the processed letter to the highload-blocks
			$arNewUids = array('UF_UID' => $arTask["ID_MESSAGE"]);
			$resAddHL = $entity_data_class::add($arNewUids);
		}

	}
	// delete temporary files
	if(!empty($arUnlinkFiles))
	{
		foreach ($arUnlinkFiles as $filePath) 
		{
			unlink($filePath);
		}
	}
}
// create tasks and attach files to them END

?>

