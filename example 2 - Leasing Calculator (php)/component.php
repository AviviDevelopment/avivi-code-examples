<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (isset($_POST["is_ajax"]) && $_POST["is_ajax"]=='Y')
	$APPLICATION->RestartBuffer();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CUser $USER */
/*

global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 180;

if($arParams["VEHICLE_TYPE_IBLOCK_ID"] && $this->StartResultCache(false))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	if(is_numeric($arParams["VEHICLE_TYPE_IBLOCK_ID"]))
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"ID" => $arParams["VEHICLE_TYPE_IBLOCK_ID"],
		));
	}
	else
	{
		$rsIBlock = CIBlock::GetList(array(), array(
			"ACTIVE" => "Y",
			"CODE" => $arParams["VEHICLE_TYPE_IBLOCK_ID"],
			"SITE_ID" => SITE_ID,
		));
	}
	if($arResult = $rsIBlock->GetNext())
	{
		$arResult["PREPAYMENT_VALUE"] = 0;
		$arResult["RESIDUAL_COST_VALUE"] = 0;
		$arResult["MONTH_PAY_VALUE"] = 0; 

		$arSort["ID"] = "ASC";
		$arFilterGlobal = array (
			"IBLOCK_LID" => SITE_ID,
			"ACTIVE" => "Y",
			"CHECK_PERMISSIONS" => "Y",
		);
		$arSelect = array(
			"ID",
			"NAME",
		);
		$arFilter = array (
			"IBLOCK_ID" => $arParams["VEHICLE_TYPE_IBLOCK_ID"],
		);
		$arItem = array();
		$arResult["VEHICLE_TYPE"] = array();
		$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false, $arSelect);
		while($arItem = $rsElement->Fetch())
		{
			$arResult["VEHICLE_TYPE"][] = $arItem;
		};

		if ($_POST["VEHICLE_TYPE"]) 
		{
			$arSelect = array(
				"ID",
			);
			$arFilter = array (
				"IBLOCK_ID" => $arParams["VEHICLE_SUBTYPE_IBLOCK_ID"],
				"PROPERTY_VEHICLE_TYPE" => $_POST["VEHICLE_TYPE"],
			);
			$arItem = array();
			$arVehicleSubtype = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false, $arSelect);
			while($arItem = $rsElement->Fetch())
			{
				$arVehicleSubtype[] = $arItem["ID"];
			};
		}

		if ($arVehicleSubtype)
		{
			$arSelect = array(
				"ID",
				"NAME",
				"PROPERTY_VEHICLE_SUBTYPE",
			);
			$arFilter = array (
				"IBLOCK_ID" => $arParams["VEHICLE_BRAND_IBLOCK_ID"],
				"PROPERTY_VEHICLE_SUBTYPE" => $arVehicleSubtype,
			);
			$arItem = array();
			$arResult["VEHICLE_BRAND"] = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false, $arSelect);
			while($arItem = $rsElement->Fetch())
			{
				$arResult["VEHICLE_BRAND"][$arItem["ID"]] = $arItem;
			};
			if ($_POST["VEHICLE_BRAND"] && !$arResult["VEHICLE_BRAND"][$_POST["VEHICLE_BRAND"]]) 
			{
				unset($_POST["VEHICLE_BRAND"]);
				unset($_POST["LEASING_PROGRAM"]);

				unset($_POST["PREPAYMENT"]);
				unset($_POST["LEASING_TERM"]);
				unset($_POST["FRANCHISE_CASCO"]);
				unset($_POST["RESIDUAL_COST"]);
			}
		}

		if ($_POST["VEHICLE_BRAND"] && $arResult["VEHICLE_BRAND"][$_POST["VEHICLE_BRAND"]])
		{
			$arSelect = array(
				"ID",
				"NAME",
				"PREVIEW_PICTURE",
				"PROPERTY_ENGINE_CAPACITY",
			);
			$arFilter = array (
				"IBLOCK_ID" => $arParams["VEHICLE_MODEL_IBLOCK_ID"],
				"PROPERTY_VEHICLE_BRAND" => $_POST["VEHICLE_BRAND"],
			);
			$arItem = array();
			$arResult["VEHICLE_MODEL"] = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false, $arSelect);
			while($arItem = $rsElement->Fetch())
			{
				$arResult["VEHICLE_MODEL"][$arItem['ID']]['ID'] = $arItem['ID'];
				$arResult["VEHICLE_MODEL"][$arItem['ID']]['NAME'] = $arItem['NAME'];
				$arResult["VEHICLE_MODEL"][$arItem['ID']]['PICTURE'] = $arItem['PREVIEW_PICTURE'];
				$arResult["VEHICLE_MODEL"][$arItem['ID']]["ENGINE_CAPACITY"][] = $arItem['PROPERTY_ENGINE_CAPACITY_VALUE'];
			};
			if ($_POST["VEHICLE_MODEL"] && !$arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]) 
			{
				unset($_POST["VEHICLE_MODEL"]);
			}
			if ($_POST["ENGINE_CAPACITY"] && !in_array($_POST["ENGINE_CAPACITY"], $arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]["ENGINE_CAPACITY"])) 
			{
				unset($_POST["ENGINE_CAPACITY"]);
			}

			if ($_POST["VEHICLE_MODEL"])
			{
				if ($arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]['PICTURE']) 
					$arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]['PICTURE'] = CFile::GetPath($arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]['PICTURE']);
				else
					$arResult["VEHICLE_MODEL"][$_POST["VEHICLE_MODEL"]]['PICTURE'] = '';
			}
		}

		if ($arVehicleSubtype && $_POST["VEHICLE_BRAND"])
		{
			$arSelect = array(
				"ID",
				"NAME",
			);
			$arFilter = array (
				"IBLOCK_ID" => $arParams["LEASING_PROGRAM_IBLOCK_ID"],
				"PROPERTY_VEHICLE_SUBTYPE_LIMIT" =>  array_merge($arVehicleSubtype, array("LOGIC" => "OR", false)),
				"PROPERTY_VEHICLE_BRAND_LIMIT" => array("LOGIC" => "OR", $_POST["VEHICLE_BRAND"], false),
			);
			$arItem = array();
			$arResult["LEASING_PROGRAM_LIST"] = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false, $arSelect);
			while($arItem = $rsElement->Fetch())
			{
				$arResult["LEASING_PROGRAM_LIST"][] = $arItem;
			};
		}

		if ($_POST["LEASING_PROGRAM"]) 
		{
			$arFilter = array (
				"IBLOCK_ID" => $arParams["LEASING_PROGRAM_IBLOCK_ID"],
				"ID" => $_POST["LEASING_PROGRAM"],
			);
			$arResult["LEASING_PROGRAM"] = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array());
			while($obElement = $rsElement->GetNextElement())
			{
				$arResult["LEASING_PROGRAM"] = $obElement->GetProperties();
			};
		
			if ($arResult["LEASING_PROGRAM"]) 
			{
				if (!$_POST["COST"])
					$_POST["COST"] = 0;

				$arFranchises = array();
				foreach ($arResult["LEASING_PROGRAM"]["TABLE_CASCO_RATES"]["VALUE"] as $key => $arFranchise) 
				{
					if (in_array($arFranchise["VEHICLE_TYPE"], $arVehicleSubtype)) 
					{
						$arFranchises[] = $arFranchise["FRANCHISE"];
					}
				}
				$arSelect = array(
					"ID",
					"NAME",
				);
				$arFilter = array (
					"IBLOCK_ID" => $arResult["LEASING_PROGRAM"]["TABLE_CASCO_RATES"]["USER_TYPE_SETTINGS"]["FRANCHISE"]["LINK_IBLOCK_ID"],
					"ID" => $arFranchises,
				);
				$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array());
				while($arItem = $rsElement->Fetch())
				{
					$arResult["FRANCHISES_VALUE"][$arItem["ID"]] = str_replace(',', '.', $arItem["NAME"]);
					if ($arResult["FRANCHISES_VALUE"][$arItem["ID"]] == $_POST["FRANCHISE_CASCO"]) 
					{
						$thisFranchiseKey = $arItem["ID"];
					}
				};

				$arTerms = array();
				$arSelect = array(
					"ID",
					"NAME",
				);
				$arFilter = array (
					"IBLOCK_ID" => $arResult["LEASING_PROGRAM"]["TABLE_INTEREST_RATES"]["USER_TYPE_SETTINGS"]["LEASING_TERM"]["LINK_IBLOCK_ID"],
				);
				$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array());
				while($arItem = $rsElement->Fetch())
				{
					$arTerms[$arItem["ID"]] = explode('-', $arItem["NAME"]);
				};
				foreach ($arTerms as $termKey => $arTerm) 
				{
					if ($arTerm[1]) 
					{
						if ($_POST["LEASING_TERM"] >= $arTerm[0] && $_POST["LEASING_TERM"] <= $arTerm[1]) 
						{
							$thisTermKey = $termKey;
							break;
						}
					}
					else
					{
						if ($_POST["LEASING_TERM"] == $arTerm[0]) 
						{
							$thisTermKey = $termKey;
							break;
						}
					}
				}

				$arPrepayments = array();
				$arSelect = array(
					"ID",
					"NAME",
				);
				$arFilter = array (
					"IBLOCK_ID" => $arResult["LEASING_PROGRAM"]["TABLE_INTEREST_RATES"]["USER_TYPE_SETTINGS"]["ADVANCE_PAYMENT"]["LINK_IBLOCK_ID"],
				);
				$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array());
				while($arItem = $rsElement->Fetch())
				{
					$arPrepayments[$arItem["ID"]] = explode('-', str_replace(',', '.', $arItem["NAME"]));
				};
				foreach ($arPrepayments as $prepaymentKey => $arPrepayment) 
				{
					if ($arPrepayment[1]) 
					{
						if ($_POST["PREPAYMENT"] >= $arPrepayment[0] && $_POST["PREPAYMENT"] <= $arPrepayment[1]) 
						{
							$thisPrepaymentKey = $prepaymentKey;
							break;
						}
					}
					else
					{
						if ($_POST["PREPAYMENT"] == $arPrepayment[0]) 
						{
							$thisPrepaymentKey = $prepaymentKey;
							break;
						}
					}
				}

				$arEngineCapacitys = array();
				$arSelect = array(
					"ID",
					"NAME",
				);
				$arFilter = array (
					"IBLOCK_ID" => $arResult["LEASING_PROGRAM"]["TABLE_GO_RATES"]["USER_TYPE_SETTINGS"]["ENGINE_CAPACITY"]["LINK_IBLOCK_ID"],
				);
				$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array());
				while($arItem = $rsElement->Fetch())
				{
					$arEngineCapacitys[$arItem["ID"]] = explode('-', str_replace(',', '.', $arItem["NAME"]));
				};
				foreach ($arEngineCapacitys as $engineCapacityKey => $arEngineCapacity) 
				{
					if ($arEngineCapacity[1]) 
					{
						if ($_POST["ENGINE_CAPACITY"] >= $arEngineCapacity[0] && $_POST["ENGINE_CAPACITY"] <= $arEngineCapacity[1]) 
						{
							$thisEngineCapacityKey = $engineCapacityKey;
							break;
						}
					}
					else
					{
						if ($_POST["ENGINE_CAPACITY"] == $arEngineCapacity[0]) 
						{
							$thisEngineCapacityKey = $engineCapacityKey;
							break;
						}
					}
				}

				$thisVehicleSubtypeKey = $arResult["VEHICLE_BRAND"][$_POST["VEHICLE_BRAND"]]["PROPERTY_VEHICLE_SUBTYPE_VALUE"];

				foreach ($arResult["LEASING_PROGRAM"]["TABLE_INTEREST_RATES"]["VALUE"] as $arInterestRates) 
				{
					if ($arInterestRates["LEASING_TERM"] == $thisTermKey && $arInterestRates["ADVANCE_PAYMENT"] == $thisPrepaymentKey) 
					{
						$T1 = str_replace(',', '.', $arInterestRates["VALUE"]);
						break;
					}
				}

				foreach ($arResult["LEASING_PROGRAM"]["TABLE_CASCO_RATES"]["VALUE"] as $arCascoRates) 
				{
					if ($arCascoRates["FRANCHISE"] == $thisFranchiseKey && $arCascoRates["VEHICLE_TYPE"] == $thisVehicleSubtypeKey) 
					{
						$T2 = str_replace(',', '.', $arCascoRates["VALUE"]);
						break;
					}
				}

				foreach ($arResult["LEASING_PROGRAM"]["TABLE_GO_RATES"]["VALUE"] as $arGoRates) 
				{
					if ($arGoRates["ENGINE_CAPACITY"] == $engineCapacityKey && $arGoRates["VEHICLE_TYPE"] == $thisVehicleSubtypeKey) 
					{
						$T3 = str_replace(',', '.', $arGoRates["VALUE"]);
						break;
					}
				}

				$arFilter = array (
					"IBLOCK_ID" => $arResult["LEASING_PROGRAM"]["FIXED_COSTS"]["LINK_IBLOCK_ID"],
					"ID" => $arResult["LEASING_PROGRAM"]["FIXED_COSTS"]["VALUE"],
				);
				$arFixedCosts = array();
				$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter,$arFilterGlobal), false, false,array("ID","IBLOCK_ID","NAME","PROPERTY_*"));
				while($obElement = $rsElement->GetNextElement())
				{
					$arItem = $obElement->GetFields();
					$arItem["PROPERTIES"] = $obElement->GetProperties();
					$arFixedCosts[] = $arItem;
				};

				$arResult["PREPAYMENT_VALUE"] = $_POST["COST"]/100*$_POST["PREPAYMENT"];
				$arResult["RESIDUAL_COST_VALUE"] = $_POST["COST"]/100*$_POST["RESIDUAL_COST"];

				$arFixCostsValues = array();
				foreach ($arFixedCosts as $arFixedCost) 
				{
					$arFixCostsValue["INCLUDED_IN_SCHEDULE"] = $arFixedCost["PROPERTIES"]["INCLUDED_IN_SCHEDULE"]["VALUE_XML_ID"];
					$arFixCostsValue["N_MONTH"] = $arFixedCost["PROPERTIES"]["N_MONTH"]["VALUE"];
					if ($arFixedCost["PROPERTIES"]["COST_TYPE"]["VALUE_XML_ID"] == 'IN_PERCENT') 
					{
						$percentValue = 0;
						switch ($arFixedCost["PROPERTIES"]["PERCENT_WHAT_CHOICE"]["VALUE_XML_ID"])
						{
							case 'COST':
								$percentValue = $_POST["COST"];
								break;
							case 'COST_PREPAYMENT':
								$percentValue = $_POST["COST"] - $arResult["PREPAYMENT_VALUE"];
								break;
							case 'TOTAL_PAYMENTS': 
								$percentValue = $_POST["COST"] - $arResult["PREPAYMENT_VALUE"] - $arResult["RESIDUAL_COST_VALUE"];
								break;
							case 'COST_WITHOUT_VAT':
								$percentValue = $_POST["COST"]*1.2;
								break;
						}

						if ($arFixedCost["PROPERTIES"]["COST_VALUE_SINGLE"]["VALUE"]) 
						{
							$arFixCostsValue["VALUE"] =  $percentValue/100*$arFixedCost["PROPERTIES"]["COST_VALUE_SINGLE"]["VALUE"];
						}
						else
						{
							$dependenceValue = 0;
							switch ($arFixedCost["PROPERTIES"]["DEPENDENCE_WHAT_CHOICE"]["VALUE_XML_ID"]) 
							{
								case 'COST_WITHOUT_VAT'
									$dependenceValue = $_POST["COST"]*1.2;
									break;
								case 'PREPAYMENT':
									$dependenceValue = $arResult["PREPAYMENT_VALUE"];
									break;
								case 'PERIOD':
									$dependenceValue = $_POST["LEASING_TERM"];
									break;
								case 'RESIDUAL_VALUE':
									$dependenceValue = $arResult["RESIDUAL_COST_VALUE"];
									break;
							}

							$multipleValue = 0;
							foreach ($arFixedCost["PROPERTIES"]["COST_VALUE_MULTIPLE"]["VALUE"] as $arMultipleCost) 
							{
								if ($arMultipleCost["NUMBER_TO"])
								{
									if ($arMultipleCost["LOGICAL_OPERATOR"] == 3435)
									{
										if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue || $arMultipleCost["NUMBER_TO"] >= $dependenceValue) 
										{
											$multipleValue = $arMultipleCost["VALUE"];
											break;
										}
									}
									else
									{
										if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue && $arMultipleCost["NUMBER_TO"] >= $dependenceValue) 
										{
											$multipleValue = $arMultipleCost["VALUE"];
											break;
										}
									}
								}
								else
								{
									if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue) 
									{
										$multipleValue = $arMultipleCost["VALUE"];
										break;
									}
								}
							}
							$arFixCostsValue["VALUE"] =  $percentValue/100*$multipleValue;
						}
					}
					elseif($arFixedCost["PROPERTIES"]["COST_TYPE"]["VALUE_XML_ID"] == 'FIXED_NUMBER')
					{
						if ($arFixedCost["PROPERTIES"]["COST_VALUE_SINGLE"]["VALUE"]) 
						{
							$arFixCostsValue["VALUE"] = $arFixedCost["PROPERTIES"]["COST_VALUE_SINGLE"]["VALUE"];
						}
						else
						{
							$multipleValue = 0;
							foreach ($arFixedCost["PROPERTIES"]["COST_VALUE_MULTIPLE"]["VALUE"] as $arMultipleCost) 
							{
								if ($arMultipleCost["NUMBER_TO"]) 
								{
									if ($arMultipleCost["LOGICAL_OPERATOR"] == 3435)
									{
										if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue || $arMultipleCost["NUMBER_TO"] >= $dependenceValue) 
										{
											$arFixCostsValue["VALUE"] = $arMultipleCost["VALUE"];
											break;
										}
									}
									else
									{
										if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue && $arMultipleCost["NUMBER_TO"] >= $dependenceValue) 
										{
											$arFixCostsValue["VALUE"] = $arMultipleCost["VALUE"];
											break;
										}
									}
								}
								else
								{
									if ($arMultipleCost["NUMBER_OF"] <= $dependenceValue) 
									{
										$arFixCostsValue["VALUE"] = $arMultipleCost["VALUE"];
										break;
									}
								}
							}
						}
					}
					$arFixCostsValues[$arFixedCost["ID"]] = $arFixCostsValue;
				}
				
				$X3 = 0;
				$X5 = 0;
				foreach ($arFixCostsValues as $key => $arFixedCostValue)
				{
					switch ($arFixedCostValue["INCLUDED_IN_SCHEDULE"]) 
					{
						case 'IN_ADVANCE':
							$X3 += $arFixedCostValue["VALUE"];
							unset($arFixCostsValues[$key]);
							break;
						case 'SPLITS_WHOLE_PERIOD':
							$X5 += $arFixedCostValue["VALUE"];
							unset($arFixCostsValues[$key]);
							break;
					}
				}
				$X3 = $X3/12;
				$X5 = $X5/$_POST["LEASING_TERM"];
				$X1 = $_POST["COST"] - $arResult["PREPAYMENT_VALUE"];
				$X1_1 = $X1 - $arResult["RESIDUAL_COST_VALUE"];
				$X2 = $_POST["LEASING_TERM"];
				$X4 = $_POST["COST"];
				$X6 = $arResult["PREPAYMENT_VALUE"];
				$X8 = $arResult["RESIDUAL_COST_VALUE"];
				$X10 = ($X1-$X8)/$X2;
				$arMonthPayments = array();
				for ($month=1; $month <= $_POST["LEASING_TERM"]; $month++)
				{
					$X5_1 = 0;
					foreach ($arFixCostsValues as $key => $arFixedCostValue)
					{
						switch ($arFixedCostValue["INCLUDED_IN_SCHEDULE"]) 
						{
							case 'SPLITS_MONTH':
								if ($arFixedCostValue["N_MONTH"] >= $month)
								{
									$X5_1 += $arFixedCostValue["VALUE"]/$arFixedCostValue["N_MONTH"];
								}
								break;
							case 'EACH_ADDED':
								if ($month%$arFixedCostValue["N_MONTH"] == 0)
								{
									$X5_1 += $arFixedCostValue["VALUE"];
								}
								break;
						}
					}
					if ($month > 1)
						$X1_1 = $X1_1 - $X10;
					if ($month <= 12)
					{
						$arMonthPayments[] = round($X10 + ($X3 + $T3 + ($X4*($T2/100)/4))/12 + (($X1_1 + $X3 + $T3 + ($X4*($T2/100)/4))*($T1/100)*30.45)/360 + ($X4*($T2/100)/12) + $T3/12 + $X5 + $X5_1,2);
					}
					else
					{
						$arMonthPayments[] = round($X10 + ($X1_1*($T1/100)*30.45)/360 + ($X4*($T2/100))/12 + $T3/12 + $X5 + $X5_1,2);
					}

				}
				$arResult["MONTH_PAYMENTS"] = $arMonthPayments;
				$arResult["MONTH_PAY_VALUE"] = round(array_sum($arMonthPayments)/count($arMonthPayments),2);
			}
		}

		$this->SetResultCacheKeys(array(
			"ID",
			"NAME",
		));
		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
	}
}
if (isset($_POST["is_ajax"]) && $_POST["is_ajax"]=='Y')
		die();
?>