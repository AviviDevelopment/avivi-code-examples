if (typeof(AV) === "undefined")
{
	AV = {};
}

if (typeof(AV.CrmBookingEditor) === "undefined")
{
	AV.CrmBookingEditor = function ()
	{
		this._settings = {};
		this._serviceUrl = '';
		this._overlay = null;
		this._bookingsTotal = [];
		this._maskMoneyOptions = {
			'prefix' : '$',
			'suffix' : '',
			'affixesStay' : true,
			'thousands' : '',
			'decimal' : '.',
			'precision' : 2,
			'allowZero' : true,
			'allowNegative': false
		};
		this._maskCommissionOptions = {
			'prefix' : '',
			'suffix' : '',
			'affixesStay' : true,
			'thousands' : '',
			'decimal' : '',
			'precision' : 0,
			'allowZero' : true,
			'allowNegative': false
		};
		this._recalcTimer = null;
		this._calcDelay = 500;
		this._errorClass = 'crm-entity-widget-content-error';
		this._errorTextClass = 'crm-entity-widget-content-error-text';
		this.mTop = 0;
		this.mLeft = 0;
	}
	AV.CrmBookingEditor.prototype =
	{
		initialize: function (settings)
		{
			this._settings = settings ? settings : {};
			this._serviceUrl = this.getSetting("serviceURL", "");

			// add supplier choice handler
			BX.addCustomEvent(
				'CrmProductSearchDialog_SelectProduct',
				BX.delegate(this.handleSupplierChoice, this)
			);

			// register click handlers
			document.getElementById('av-bookings').onclick = BX.delegate(this.handleClick, this);

			// register change handlers
			document.getElementById('av-bookings').onchange = BX.delegate(this.handleChange, this);

			// register keyup handlers
			document.getElementById('av-bookings').onkeyup = BX.delegate(this.handleKeyup, this);

			// add masks to inputs
			this.createInputMask();

			//add additional js
			var _crmPickerJSURL = this.getSetting('crmPickerJSURL', '');
			if(_crmPickerJSURL && typeof(BX.CrmEntitySelector) == 'undefined')
			{
				BX.loadScript(this.getSetting('crmPickerJSURL', ''));
				return;
			}

			this.initSortable();
			this.animMouseControl();
		},
		handleMouseOut: function(e)
		{
		},
		handleMouseOver: function(e)
		{
		},
		handleChange: function(e)
		{
			switch (true) {
				case e.target.tagName == 'SELECT' && e.target.name == 'UF_CANCEL_PENALTY':
			    	this.handleUnderPenaltyChange(e.target);
			    	break;
			    case e.target.tagName == 'SELECT' && e.target.name == 'UF_CANCEL_PROTECTED':
			    	this.handleInsuranceProtectionChange(e.target);
			    	break;
			}
		},
		handleKeyup: function(e)
		{
			var moneyInputs = this.getSetting('moneyInputs', []);

			switch (true) {
				case e.target.tagName == 'INPUT' && e.target.name == 'UF_COMMISSION':
			    	this.handleComissionFieldKeyup(e.target);
			    	break;
			    case e.target.tagName == 'INPUT' && moneyInputs.includes(e.target.name):
			    	this.handleMoneyFieldKeyup(e.target);
			    	break;
			}
		},
		handleClick: function(e)
		{
			var classList = Object.values(e.target.classList);
			var elID = e.target.id;

			// check field for being in cancel block
			this.handleCancelFieldErrorCheck(e.target);

			// handle clicks
			switch (true) {
				case elID == this.getSetting('addBookingBtn', ''):
					this.handleAddBookingButtonClick(e);
					break;
				case elID == this.getSetting('saveBookingsBtn', ''):
					this.handleSaveBookingsButtonClick(e);
					break;
				case classList.includes('av-booking-remove-payment-button'):
					this.handleRemovePaymentButtonClick(e.target);
			        break;
			    case classList.includes('av-booking-new-payment-button'):
					this.handleNewPaymentButtonClick(e.target);
			        break;
			    case classList.includes('av-commission-type'):
					this.handleCommissionTypeButtonClick(e.target);
			        break;
			    case classList.includes('av-remove-booking'):
					this.handleRemoveBookingButtonClick(e.target);
			        break;
			    case classList.includes('open_button'):
					this.handleToggleButtonClick(e.target);
			        break;
			    case classList.includes('fa-arrows-alt') 
			    	|| (e.target.nodeName == "path" && Object.values(e.target.parentNode.classList).includes('fa-arrows-alt')):
					this.hideBookings();
			        break;
			    case e.target.tagName == 'INPUT' && e.target.name == 'UF_CANCEL_BOOKING':
			    	this.handleCancelButtonClick(e.target);
			    	break; 
			}
		},
		handleToggleButtonClick: function(toggleBtn)
		{
			var booking = toggleBtn.closest('.av-booking');
			toggleBtn.classList.toggle("closed");

			var widgetContent = booking.querySelector('.crm-entity-widget-content');
			widgetContent.classList.toggle("hidden");

			var titleContent = booking.querySelector('.crm-entity-card-widget-title');
			titleContent.classList.toggle("sortable");

			this.disableDrag();
		},
		handleMoneyFieldKeyup: function(moneyField)
		{
			this.recalculateBookings();
		},
		handleComissionFieldKeyup: function(commissionField)
		{
			var commissionType = commissionField.closest('div').querySelector('input[name=UF_COMMISSION_TYPE]').value;
			if(commissionType == "P")
			{
				var comissionValue = parseInt(commissionField.value.slice(0, 2));
				commissionField.value = (comissionValue < 0 || isNaN(comissionValue)) ? 0 : comissionValue;
			}
			// call recalculation
			this.recalculateBookings();
		},
		handleRemoveBookingButtonClick: function(removeBookingButton)
		{
			var bookingBlock = removeBookingButton.closest('.av-booking');
			var that = this;
			var removeBookingDialog = BX.PopupWindowManager.create("remove-confirm", null, {
	            content: BX.CrmBookingEditorMessages['removeBookingDialogText'],
	            width: 400, // ширина окна
	            height: 200, // высота окна
	            zIndex: 100, // z-index
	            closeIcon: {
	                // объект со стилями для иконки закрытия, при null - иконки не будет
	                opacity: 1
	            },
	            titleBar: BX.CrmBookingEditorMessages['removeBookingDialogTitle'],
	            closeByEsc: true, // закрытие окна по esc
	            darkMode: false, // окно будет светлым или темным
	            autoHide: false, // закрытие при клике вне окна
	            draggable: true, // можно двигать или нет
	            resizable: false, // можно ресайзить
	            min_height: 100, // минимальная высота окна
	            min_width: 100, // минимальная ширина окна
	            lightShadow: true, // использовать светлую тень у окна
	            angle: false, // появится уголок
	            overlay: {
	                // объект со стилями фона
	                backgroundColor: 'black',
	                opacity: 500
	            }, 
	            buttons: [
	                new BX.PopupWindowButton({
	                    text: BX.CrmBookingEditorMessages['removeBookingDialogConfirmBtn'], // текст кнопки
	                    className: 'popup-window-button-accept', // доп. классы
	                    events: {
	                    	click: function() {
	                    		bookingBlock.remove();
	                    		removeBookingDialog.close();

	                    		// recalculate totals
	                    		that.recalculateBookings();

	                    		if(document.querySelectorAll('.av-booking').length == 0)
	                    		{
	                    			that.toggleBottomBar();
	                    		}
	                    	}
	                    }
	                }),
	                new BX.PopupWindowButton({
	                    text: BX.CrmBookingEditorMessages['removeBookingDialogCancelBtn'],
	                    className: 'popup-window-button-link popup-window-button-link-cancel',
	                    events: {
	                    	click: function() {
	                    		removeBookingDialog.close();
	                    	}
	                    }
	                })
	            ],
	            events: {
	               	onPopupShow: function() {
	                	// Событие при показе окна
	               	},
	               	onPopupClose: function(popupWindow) {
	                	// Событие при закрытии окна
	                	popupWindow.destroy(); 
	               	}
	            }
	        });

	        removeBookingDialog.show();
		},
		handleRemovePaymentButtonClick: function(removePaymentButton)
		{
			var paymentBlock = removePaymentButton.closest('.av-booking-payment-fields');
			var bookingBlock = paymentBlock.closest('.av-booking');
			if(bookingBlock.querySelectorAll('.av-booking-payment-fields').length > 1)
			{
				paymentBlock.remove();
			}
			else
			{
				paymentBlock.querySelector('input[name="UF_PAYMENT_AMOUNT[]"]').value = "$0.00";
				paymentBlock.querySelector('input[name="UF_PAYMENT_DATE[]"]').value = this.getSetting("currentDate", "");
			}

			// call recalculation
			this.recalculateBookings();

		},
		handleNewPaymentButtonClick: function(newPaymentButton)
		{
			var paymentBlock = newPaymentButton.parentNode.previousElementSibling;
			var newPaymentBlock = paymentBlock.cloneNode(true);

			newPaymentBlock.querySelector('input[name="UF_PAYMENT_AMOUNT[]"]').value = 0;
			newPaymentBlock.querySelector('input[name="UF_PAYMENT_DATE[]"]').value = this.getSetting("currentDate", "");
			// paymentBlock.insertAdjacentHTML('afterEnd', newPaymentBlock.outerHTML); //not working with changed content
			paymentBlock.after(newPaymentBlock);

			// add masks to inputs
			this.createInputMask(["UF_PAYMENT_AMOUNT[]"]);

		},
		handleCancelButtonClick: function(cancelInput)
		{
			var cancelBlock = cancelInput.closest('.av-booking-cancel-block').nextElementSibling;
			cancelBlock.style.display = (cancelInput.checked) ? 'flex' : 'none';

			// call recalculation
			this.recalculateBookings();
		},
		handleCommissionTypeButtonClick: function(commissionTypeButton)
		{
			var bookingBlock = commissionTypeButton.closest('.av-booking');
			var bookingCostPercent = this.clearMoney(bookingBlock.querySelector('input[name=UF_PACKAGE_COST]').value) / 100;
			var commissionTypeInput = commissionTypeButton.nextElementSibling;
			var commissionInput = commissionTypeButton.previousElementSibling;
			var commissionType = commissionTypeInput.value;
			var commissionValue = (commissionType == "P") ? parseInt(commissionInput.value) : this.clearMoney(commissionInput.value);

			commissionTypeInput.value = (commissionType == 'P') ? 'V' : 'P';
			commissionTypeButton.textContent = (commissionType == 'P') ? '$' : '%';
			
			// recalculate value depending on commission type
			commissionInput.value = (commissionTypeInput.value == 'P') 
				? Math.round(commissionValue / bookingCostPercent)
					: (bookingCostPercent * commissionValue).toFixed(2);

			// add masks to inputs
			this.createInputMask(["UF_COMMISSION"]);

			// call recalculation
			this.recalculateBookings();
		},
		handleSupplierChoice: function(supplierID)
		{
			supplierID = parseInt(supplierID);
			if (supplierID <= 0)
			{
				return;
			}
			// TODO: render product here
			BX.ajax({
				'url': this._serviceUrl,
				'method': 'POST',
				'dataType': 'json',
				'data':
				{
					'ACTION': 'ADD_SUPPLIER',
					'SUPPLIER_ID': supplierID
				},
				onsuccess: BX.delegate(this.onSupplierChoiceSuccess, this)
			});
		},
		handleSaveBookingsButtonClick: function(e)
		{
			e.preventDefault();
			var saveBookingsBtn = e.target;
			var isError = false;
			var bookingsList = document.querySelectorAll('.av-booking');
			var bookingRequiredFields = this.getSetting("bookingRequiredFields", []);
			var moneyInputs = this.getSetting('moneyInputs', []);

			// Avivi: check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
			var unicConfirmNumbers = [];
			var doubleConfirmNumbers = [];

			// add animation to button
			saveBookingsBtn.classList.add('ui-btn-clock');

			if(bookingsList.length > 0)
			{
				for(var index = 0; index < bookingsList.length; index++)
				{
					// check required cancel block fields
					var isCancelBlock = bookingsList[index].querySelector('input[name=UF_CANCEL_BOOKING]').checked;
					if(isCancelBlock)
					{
						var cancelBlockFields = bookingsList[index].querySelector('.av-booking-cancel-container').querySelectorAll('input, select');
						if(cancelBlockFields.length)
						{
							for (var key = 0; key < cancelBlockFields.length; key++)
							{

								if((cancelBlockFields[key].tagName == 'SELECT' && cancelBlockFields[key].options[cancelBlockFields[key].selectedIndex].value == '')
									|| (cancelBlockFields[key].tagName == 'INPUT' && this.clearMoney(cancelBlockFields[key].value) == 0.00 && !cancelBlockFields[key].disabled))
								{
									isError = true;
									var fieldWrapper = cancelBlockFields[key].closest('.crm-entity-widget-content-block');
									this.createFieldError(fieldWrapper);
								}
							}
						}
					}

					// check required booking fields
					if(bookingRequiredFields.length > 0)
					{
						for (var key = 0; key < bookingRequiredFields.length; key++)
						{
							var fieldToCkeck = bookingsList[index].querySelector('input[name="'+bookingRequiredFields[key]+'"]');
							// var fieldValue = fieldToCkeck.value;
							var fieldValue = fieldToCkeck.value.trim();


							if(!fieldValue || fieldValue == '' 
								|| (moneyInputs.includes(bookingRequiredFields[key]) &&  this.clearMoney(fieldValue) == 0.00))
							{
								isError = true;
								var fieldWrapper = fieldToCkeck.closest('.crm-entity-widget-content-block');
								this.createFieldError(fieldWrapper);
							}

							// Avivi: check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
							if(bookingRequiredFields[key] == "UF_CONFIRM_NUMBER")
							{
								if(fieldValue && fieldValue != '')
								{
									if(!unicConfirmNumbers.includes(fieldValue))
									{
										unicConfirmNumbers.push(fieldValue);
									}
									else
									{
										doubleConfirmNumbers.push(fieldValue);
									}
								}
							}
							// Avivi: END check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
						}

					}
				}

				// Avivi: check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
				if(doubleConfirmNumbers.length > 0)
				{
					for(var index = 0; index < bookingsList.length; index++)
					{
						var fieldToCkeck = bookingsList[index].querySelector('input[name="UF_CONFIRM_NUMBER"]');
						var fieldValue = fieldToCkeck.value.trim();

						if(doubleConfirmNumbers.includes(fieldValue))
						{
							/*isError = true;
							var fieldWrapper = fieldToCkeck.closest('.crm-entity-widget-content-block');
							this.createFieldError(fieldWrapper, BX.CrmBookingEditorMessages['doubleFieldError']);*/
						}
					}
				}
				// Avivi: END check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
			}
			else
			{
				isError = true;
			}

			if(!isError)
			{
				if(bookingsList.length > 0)
				{
					var bookingRows = [];
					for(var index = 0; index < bookingsList.length; index++)
					{
						var bookingBlock = bookingsList[index];
						var bookingRow = this.serializeNode(bookingBlock);
						if(bookingRow)
						{
							bookingRows.push(this.serializeNode(bookingBlock));
						}
					}
					
					// send ajax to save/update rows
					if(bookingRows.length)
					{
						BX.ajax({
							'url': this._serviceUrl,
							'method': 'POST',
							'dataType': 'json',
							'data':
							{
								'ACTION': 'SAVE_BOOKINGS',
								'INQUIRY_ID': this.getSetting("inquiryID", ''),
								'BOOKINGS': bookingRows
							},
							onsuccess: BX.delegate(this.onBookingsSaveSuccess, this)
						});
					}
				}
			}
			else
			{
				// scroll to first error
				this.scrollToError();
				// remove animation from button
				saveBookingsBtn.classList.remove('ui-btn-clock');
			}
		},
		handleAddBookingButtonClick: function(e)
		{
			e.preventDefault();
			this.createOverlay(995);

			var caller = 'crm_productrow_list';
			var jsEventsManagerId = this.getSetting("jsEventsManagerId", "");
			var suppliersURL = this.getSetting("suppliersURL", "");
			var dlg = BX.CrmProductSearchDialogWindow.create(
				{
					content_url: suppliersURL +
						"?caller=" + caller + "&JS_EVENTS_MANAGER_ID=" + BX.util.urlencode(jsEventsManagerId) +
						"&sessid=" + BX.bitrix_sessid(),
					closeWindowHandler: BX.delegate(this.handleChoiceProductDialogClose, this),
					showWindowHandler: BX.delegate(this.handleChoiceProductDialogShow, this),
					jsEventsManagerId: jsEventsManagerId,
					height: Math.max(500, window.innerHeight - 400),
					width: Math.max(800, window.innerWidth - 400),
					minHeight: 500,
					minWidth: 800,
					draggable: true,
					resizable: true
				}
			);
			dlg.show();
		},
		handleChoiceProductDialogClose: function()
		{
		},
		handleChoiceProductDialogShow: function()
		{
			this.removeOverlay();
		},
		handleCancelFieldErrorCheck(field)
		{
			// is cancel field
			// if(field.closest('div.av-booking-cancel-container') !== null && field.tagName == 'INPUT')

			// allow to remove error from any bookung place, not only from cancel block
			/* if(field.closest('.av-booking-cancel-container') !== null)
			{*/
				var fieldWrapper = field.closest('.crm-entity-widget-content-block');
				this.removeFieldError(fieldWrapper);
			/*}*/
		},
		handleInsuranceProtectionChange(insuranceProtectionField)
		{
			this.recalculateBookings();
		},
		handleUnderPenaltyChange(underPenaltyField)
		{
			var cancelBlock = underPenaltyField.closest('.av-booking-cancel-container');
			var refundPenaltyField = cancelBlock.querySelector('input[name="UF_CANCEL_REFUND"]');
			if(underPenaltyField.value != 'Y')
			{
				refundPenaltyField.setAttribute("disabled", "disabled");
			}
			else
			{
				refundPenaltyField.removeAttribute("disabled");
			}

			//remove error from refund field
			var fieldWrapper = refundPenaltyField.closest('.crm-entity-widget-content-block');
			this.removeFieldError(fieldWrapper);

			// call recalculation
			this.recalculateBookings();
		},
		onSupplierChoiceSuccess: function(response)
		{
			// var data;
			if (response && response["ERROR"] == "" && response["data"])
			{
				var bookingsBlock = document.querySelector('.av-bookings-list');
				var bookingsList = document.querySelectorAll('.av-booking');
				var lastBooking = document.querySelector('.av-booking:nth-last-of-type(1)');

				if(bookingsList.length > 0)
				{
					lastBooking.insertAdjacentHTML('afterEnd', response["data"]);
				}
				else
				{
					bookingsBlock.innerHTML = response["data"];

					this.toggleBottomBar();
				}

				// add masks to inputs
				this.createInputMask();

				// run block scripts
				var lastBooking = document.querySelector('.av-booking:nth-last-of-type(1)');

				// run crm picker scripts
				runScripts(lastBooking);
			}
		},
		onBookingsSaveSuccess: function(response)
		{
			if (response)
			{
				if(response["ERROR"] == "" && response["data"])
				{
					var bookingsBlock = document.getElementById('av-bookings');
					
					// set bookings list
					bookingsBlock.outerHTML = response["data"];

					// run crm picker scripts
					bookingsBlock = document.getElementById('av-bookings');
					runScripts(bookingsBlock);

					// reinitialize this object
					this.initialize(this._settings);
				}
				// Avivi: check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
				else if(response["ERROR"] == "doubles" && response["doubles"])
				{
					var saveBookingsBtn = document.getElementById(this.getSetting('saveBookingsBtn', ''));
					saveBookingsBtn.classList.remove('ui-btn-clock');

					var bookingsList = document.querySelectorAll('.av-booking');
					var isError = false;

					if(response["doubles"].length > 0 && bookingsList.length > 0)
					{
						for(var index = 0; index < bookingsList.length; index++)
						{
							var fieldToCkeck = bookingsList[index].querySelector('input[name="UF_CONFIRM_NUMBER"]');
							var fieldValue = fieldToCkeck.value.trim();

							if(response["doubles"].includes(fieldValue))
							{
								isError = true;
								var fieldWrapper = fieldToCkeck.closest('.crm-entity-widget-content-block');
								this.createFieldError(fieldWrapper, BX.CrmBookingEditorMessages['doubleFieldError']);
							}
						}
						if(isError)
						{
							this.scrollToError();
						}
					}
				}
				// Avivi: END check UF_CONFIRM_NUMBER doubles for current inquiry (deal)
			}
		},
		toggleBottomBar: function()
		{
			var totalBookingsContainer = document.getElementById(this.getSetting('totalBookingsContainer', ''));
			var saveBookingsBtn = document.getElementById(this.getSetting('saveBookingsBtn', ''));

			totalBookingsContainer.style.display = (totalBookingsContainer.style.display == 'none') ? '' : 'none';
			saveBookingsBtn.style.display = (saveBookingsBtn.style.display == 'none') ? '' : 'none';
		},
		createInputMask: function(moneyInputs)
		{	
			if(moneyInputs == undefined)
			{
				var moneyInputs = this.getSetting('moneyInputs', []);
			}

			if(moneyInputs.length)
			{
				var that = this;
				for (var i = 0; i < moneyInputs.length; i++)
				{
					inputs = document.querySelectorAll('input[name="'+moneyInputs[i]+'"]');
					for(var index = 0; index < inputs.length; index++)
					{
						var mask = that._maskMoneyOptions;
						if(moneyInputs[i] == "UF_COMMISSION")
						{
							var commissionType = inputs[index].closest('div').querySelector('input[name=UF_COMMISSION_TYPE]').value;
							if(commissionType == "P")
							{
								mask = that._maskCommissionOptions;
							}
						}

						inputs[index].value = (inputs[index].value == "") ? 0 : inputs[index].value;
						$(inputs[index]).maskMoney(mask);
						$(inputs[index]).maskMoney('mask', inputs[index].value);
					}
				}
			}

		},
		createFieldError: function(fieldWrapper, errorText)
		{
			var errorClass = this._errorClass;
			var errorTextClass = this._errorTextClass;

			if(typeof(errorText) == "undefined")
			{
				errorText = BX.CrmBookingEditorMessages['requiredFieldError'];
			}

			if(!fieldWrapper.classList.contains(errorClass))
			{
				var errorHtml = BX.create('div', {
				    attrs: {
				        className: errorTextClass
				    },
				    text: errorText
				});
				// add error class
				fieldWrapper.classList.add(errorClass);
				// add error class
				fieldWrapper.appendChild(errorHtml);
			}
		},
		removeFieldError: function(fieldWrapper)
		{
			var errorClass = this._errorClass;
			var errorTextClass = this._errorTextClass;
			if(fieldWrapper !== null && fieldWrapper.classList.contains(errorClass))
			{
				// remove error class
				fieldWrapper.classList.remove(errorClass);
				// remove error text
				var errorText = fieldWrapper.querySelector('.crm-entity-widget-content-error-text');
				if(errorText)
				{
					errorText.remove();
				}
			}
		},
		findScrollPosition: function(obj)
		{
			var curtop = 0;
			if (obj.offsetParent)
			{
				do
				{
					curtop += obj.offsetTop;
				}
				while 
					(obj = obj.offsetParent);
				return [curtop - 50];
			}
		},
		scrollToError: function()
		{
			// scroll to element
			var that = this;
			// Avivi: open hidden blocks, to avoid invisible errors
			this.showBookings();

			setTimeout(function(){
				// scrol to element with positioning
				window.scroll(0, that.findScrollPosition(document.querySelector('.crm-entity-widget-content-error')));
			},50);
		},
		disableDrag: function()
		{
			var bookings = document.querySelectorAll('.av-booking');
			bookings.forEach(function(booking) {
				var dragBtn = booking.querySelector('.fa-arrows-alt');
				dragBtn.classList.remove('drag');
				dragBtn.setAttribute("title", BX.CrmBookingEditorMessages.dragBtnTitleClick);
			});
		},
		showBookings: function()
		{
			var bookings = document.querySelectorAll('.av-booking');
			bookings.forEach(function(booking) {
				var toggleBtn = booking.querySelector('.open_button');
				toggleBtn.classList.remove("closed");

				var widgetContent = booking.querySelector('.crm-entity-widget-content');
				widgetContent.classList.remove("hidden");

				var titleContent = booking.querySelector('.crm-entity-card-widget-title');
				titleContent.classList.remove("sortable");
			});
		},
		hideBookings: function()
		{
			var bookings = document.querySelectorAll('.av-booking');
			bookings.forEach(function(booking) {
				var dragBtn = booking.querySelector('.fa-arrows-alt');
				var dragBtnClassList = Object.values(dragBtn.classList);
				if(!dragBtnClassList.includes('drag'))
				{
					// enable drag
					dragBtn.classList.add("drag");
					dragBtn.setAttribute("title", BX.CrmBookingEditorMessages.dragBtnTitleDrag);

					var toggleBtn = booking.querySelector('.open_button');
					toggleBtn.classList.add("closed");

					var widgetContent = booking.querySelector('.crm-entity-widget-content');
					widgetContent.classList.add("hidden");

					var titleContent = booking.querySelector('.crm-entity-card-widget-title');
					titleContent.classList.add("sortable");
				}
			});
		},
		getSetting: function (name, defaultval)
		{
			return this._settings.hasOwnProperty(name) ? this._settings[name] : defaultval;
		},
		recalculateTotals: function ()
		{
			var bookingsTotal = this._bookingsTotal;
			var cost = 0, paid = 0, balance = 0, commission = 0, refund = 0;
			if(bookingsTotal.length)
			{
				for (var num = 0; num < bookingsTotal.length; num++)
				{
					cost += bookingsTotal[num].cost;
					paid += bookingsTotal[num].paid;
					balance += bookingsTotal[num].balance;
					commission += bookingsTotal[num].commission;
					refund += bookingsTotal[num].refund;
				}
			}
			
			var totals = {cost : cost, paid : paid, balance : balance, commission : commission, refund: refund};
			this.setTotal(totals);
		},
		recalculateBookings: function ()
		{
			if (this._recalcTimer)
			{
				clearTimeout(this._recalcTimer);
			}

			this._recalcTimer = setTimeout(BX.delegate(function()
			{
				var bookingsList = document.querySelectorAll('.av-booking');
				this._bookingsTotal = [];
				if(bookingsList.length > 0)
				{
					for (var num = 0; num < bookingsList.length; num++)
					{
						var cost = 0, paid = 0, balance = 0, commission = 0, refund = 0;
						var paymentsList = bookingsList[num].querySelectorAll('input[name="UF_PAYMENT_AMOUNT[]"]');
						var commissionType = bookingsList[num].querySelector('input[name=UF_COMMISSION_TYPE]').value;
						var isCancelActive = bookingsList[num].querySelector('input[name=UF_CANCEL_BOOKING]').checked;
						var isUnderPenalty = (bookingsList[num].querySelector('select[name=UF_CANCEL_PENALTY]').value == "Y") ? true : false;
						var isProtectedByInsurance = (bookingsList[num].querySelector('select[name=UF_CANCEL_PROTECTED]').value == "Y") ? true : false;
						var refundValue = this.clearMoney(bookingsList[num].querySelector('input[name=UF_CANCEL_REFUND]').value);
						var commissionValue = (commissionType == "P") 
							? parseInt(bookingsList[num].querySelector('input[name=UF_COMMISSION]').value) 
								: this.clearMoney(bookingsList[num].querySelector('input[name=UF_COMMISSION]').value);

						// cost
						cost = this.clearMoney(bookingsList[num].querySelector('input[name=UF_PACKAGE_COST]').value);
						// paid
						if(paymentsList.length)
						{
							for (var index = 0; index < paymentsList.length; index++)
							{
								paid += this.clearMoney(paymentsList[index].value);
							}
						}
						// balance
						balance = cost - paid;
						// commission
						commission = (commissionType == "P") ? parseFloat(((cost / 100) * commissionValue).toFixed(2)) : commissionValue;
						//refund if canceled
						if(isCancelActive)
						{
							// cancel commission if not protected
							if(!isProtectedByInsurance)
							{
								commission = 0;
							}

							refund = paid;
							// panalty substruction
							if(isUnderPenalty)
							{
								refund -= refundValue;
							}
						}

						var bookingTotals = {cost : cost, paid : paid, balance : balance, commission : commission, refund: refund};

						this._bookingsTotal.push(bookingTotals);
						this.setBookingTotal(bookingsList[num], bookingTotals);
					}
				}

				this.recalculateTotals();

			}, this),
			this._calcDelay);
		},
		setBookingTotal: function (bookingNode, bookingTotals)
		{
			for (var index in bookingTotals)
			{
				var bookingTotalItem = bookingNode.querySelector('.av_bookings_supplier_'+index);

				// show/hide refund value
				if(index == "refund")
				{
					var bookingTotalItemWrapper = bookingTotalItem.closest('tr');
					bookingTotalItemWrapper.style.display = (bookingTotals[index] > 0) ? '' : 'none';
				}

				// set value
				bookingTotalItem.textContent = this.currencyFormat(bookingTotals[index]);
			}
		},
		setTotal: function (totals)
		{
			var bookingsList = document.getElementById('av-bookings');
			for (var index in totals)
			{
				var totalItem = bookingsList.querySelector('.av_bookings_'+index);

				// show/hide refund value
				if(index == "refund")
				{
					var totalItemWrapper = totalItem.closest('tr');
					totalItemWrapper.style.display = (totals[index] > 0) ? '' : 'none';
				}

				// set value
				totalItem.textContent = this.currencyFormat(totals[index]);
			}
		},
		clearMoney: function(value)
		{
			// return parseInt(value.replace(/\D/g,'')) || 0;
			return parseFloat(value.replace(/(^\$)|(\s)/g, '')) || 0.00;
		},
		currencyFormat: function(value)
		{
			return currency(value, {separator: '', decimal: '.', precision: 2}).format();
		},
		createOverlay: function(zIndex)
		{
			zIndex = parseInt(zIndex);
			if (!this._overlay)
			{
				var windowSize = BX.GetWindowScrollSize();
				this._overlay = document.body.appendChild(BX.create("DIV", {
					style: {
						position: 'absolute',
						top: '0px',
						left: '0px',
						zIndex: zIndex || (parseInt(this.DIV.style.zIndex)-2),
						width: windowSize.scrollWidth + "px",
						height: windowSize.scrollHeight + "px"
					}
				}));
				BX.unbind(window, 'resize', BX.proxy(this._resizeOverlay, this));
				BX.bind(window, 'resize', BX.proxy(this._resizeOverlay, this));
			}
		},
		removeOverlay: function()
		{
			if (this._overlay && this._overlay.parentNode)
			{
				this._overlay.parentNode.removeChild(this._overlay);
				BX.unbind(window, 'resize', BX.proxy(this._resizeOverlay, this));
				this._overlay = null;
			}
		},
		_resizeOverlay: function()
		{
			var windowSize = BX.GetWindowScrollSize();
			this._overlay.style.width = windowSize.scrollWidth + "px";
		},
		serializeNode: function (node)
		{
			// Setup our serialized data
			var nodefields = node.querySelectorAll('input, select, textarea');
			var moneyInputs = this.getSetting('moneyInputs', []);

			var serialized = [];
			// Loop through each field in the form
			for (var i = 0; i < nodefields.length; i++)
			{
				var field = nodefields[i];
				var fieldValue = '';

				if (!field.name || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button')
					continue;

				// else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked)
				else if (field.type !== 'checkbox' && field.type !== 'radio')
				{
					fieldValue = (moneyInputs.includes(field.name)) ? this.clearMoney(field.value) : field.value;
				}
				// add checkbox condition
				else if(field.type == 'checkbox')
				{
					fieldValue = (field.checked) ? 1 : 0;
				}

				// check field for being multiple, add value
				if(field.name.indexOf('[]') !== -1)
				{
					var fieldKey = field.name.replace('[]', '');
					if(serialized[fieldKey] == undefined)
					{
						serialized[fieldKey] = [];
					}

					serialized[fieldKey].push(fieldValue);
				}
				else
				{
					serialized[field.name] = fieldValue;
				}
			}
			return serialized;
		},
		animMouseControl: function()
		{
			var that = this;
			document.onmousemove = function(e){
				mouse_X = e.pageX;
				mouse_Y = e.pageY;
				that.mTop = mouse_Y + 10 + 'px';
				that.mLeft = mouse_X + 10 + 'px';
			};
		},
		initSortable: function()
		{
			// init sortable
			var sortable = document.querySelector('.av-bookings-list');
			var that = this;
			// var sortable = Sortable.create(el);
			new Sortable(sortable, {
				// swap: true, // Enable swap plugin
				// swapClass: 'highlight', // The class applied to the hovered swap item
				animation: 300,
				// delay: 300,
				// filter: ".av-booking",
				forceFallback: true,
				ghostClass: "sortable-ghost",
				chosenClass: "sortable-chosen",
				dragClass: "sortable-drag",
				handle: '.drag',
				onStart: function (evt) {
				},
				onClone: function (/**Event*/evt) {
				},
				onMove: function (/**Event*/evt, /**Event*/originalEvent) {
				},
				onEnd: function (evt) {
				}
			});
		}
	}

	AV.CrmBookingEditor.create = function(settings)
	{
		var self = new AV.CrmBookingEditor();
		self.initialize(settings);
		return self;
	};
}

if (typeof(BX.CrmProductSearchDialogWindow) === "undefined")
{
	BX.CrmProductSearchDialogWindow = function()
	{
		this._settings = {};
		this.popup = null;
		this.random = "";
		this.contentContainer = null;
		this.zIndex = 996;
		this.jsEventsManager = null;
		this.pos = null;
		this.height = 0;
		this.width = 0;
		this.resizeCorner = null;
	};

	BX.CrmProductSearchDialogWindow.prototype = {
		initialize: function (settings)
		{
			this.random = Math.random().toString().substring(2);

			this._settings = settings ? settings : {};

			var size = BX.CrmProductSearchDialogWindow.size;

			this._settings.width = size.width || this._settings.width || 1100;
			this._settings.height = size.height || this._settings.height || 530;
			this._settings.minWidth = this._settings.minWidth || 500;
			this._settings.minHeight = this._settings.minHeight || 800;
			this._settings.draggable = !!this._settings.draggable || true;
			this._settings.resizable = !!this._settings.resizable || true;
			if (typeof(this._settings.closeWindowHandler) !== "function")
				this._settings.closeWindowHandler = null;
			if (typeof(this._settings.showWindowHandler) !== "function")
				this._settings.showWindowHandler = null;

			this.jsEventsManager = BX.Crm[this._settings.jsEventsManagerId] || null;

			this.contentContainer = BX.create(
				"DIV",
				{
					attrs: {
						className: "crm-catalog",
						style: "display: block; background-color: #f3f6f7; height: " + this._settings.height +
							"px; overflow: hidden; width: " + this._settings.width + "px;"
					}
				}
			);
		},
		_handleCloseDialog: function(popup)
		{
			if(popup)
				popup.destroy();
			this.popup = null;
			if (this.jsEventsManager)
			{
				this.jsEventsManager.unregisterEventHandlers("CrmProduct_SelectSection");
			}
			if (typeof(this._settings.closeWindowHandler) === "function")
				this._settings.closeWindowHandler();
		},
		_handleAfterShowDialog: function(popup)
		{
			popup.popupContainer.style.position = "fixed";
			popup.popupContainer.style.top =
				(parseInt(popup.popupContainer.style.top) - BX.GetWindowScrollPos().scrollTop) + 'px';
			if (typeof(this._settings.showWindowHandler) === "function")
				this._settings.showWindowHandler();
		},
		setContent: function (htmlData)
		{
			if (BX.type.isString(htmlData) && BX.type.isDomNode(this.contentContainer))
				this.contentContainer.innerHTML = htmlData;
		},
		show: function ()
		{
			BX.ajax({
				method: "GET",
				dataType: 'html',
				url: this._settings.content_url,
				data: {},
				skipAuthCheck: true,
				onsuccess: BX.delegate(function(data) {
					this.setContent(data || "&nbsp;");
					this.showWindow();
				}, this),
				onfailure: BX.delegate(function() {
					if (typeof(this._settings.showWindowHandler) === "function")
						this._settings.showWindowHandler();
				}, this)
			});
		},
		showWindow: function ()
		{
			this.popup = new BX.PopupWindow(
				"CrmProductSearchDialogWindow_" + this.random,
				null,
				{
					overlay: {opacity: 82},
					autoHide: false,
					draggable: this._settings.draggable,
					offsetLeft: 0,
					offsetTop: 0,
					bindOptions: { forceBindPosition: false },
					bindOnResize: false,
					zIndex: this.zIndex - 1100,
					closeByEsc: true,
					closeIcon: { top: '10px', right: '15px' },
					titleBar: BX.CrmBookingEditorMessages['supplierSearchDialogTitle'],
					events:
					{
						onPopupClose: BX.delegate(this._handleCloseDialog, this),
						onAfterPopupShow: BX.delegate(this._handleAfterShowDialog, this)
					},
					content: this.contentContainer
				}
			);
			if (this.popup.popupContainer)
			{
				this.resizeCorner = BX.create(
					'SPAN',
					{
						attrs: {className: "bx-crm-dialog-resize"},
						events: {mousedown : BX.delegate(this.resizeWindowStart, this)}
					}
				);
				this.popup.popupContainer.appendChild(this.resizeCorner);
				if (!this._settings.resizable)
					this.resizeCorner.style.display = "none";
			}
			this.popup.show();
		},
		setResizable: function(resizable)
		{
			resizable = !!resizable;
			if (this._settings.resizable !== resizable)
			{
				this._settings.resizable = resizable;
				if (this.resizeCorner)
				{
					if (resizable)
						this.resizeCorner.style.display = "inline-block";
					else
						this.resizeCorner.style.display = "none";
				}
			}
		},
		resizeWindowStart: function(e)
		{
			if (!this._settings.resizable)
				return;

			e =  e || window.event;
			BX.PreventDefault(e);

			this.pos = BX.pos(this.contentContainer);

			BX.bind(document, "mousemove", BX.proxy(this.resizeWindowMove, this));
			BX.bind(document, "mouseup", BX.proxy(this.resizeWindowStop, this));

			if (document.body.setCapture)
				document.body.setCapture();

			try { document.onmousedown = false; } catch(e) {}
			try { document.body.onselectstart = false; } catch(e) {}
			try { document.body.ondragstart = false; } catch(e) {}
			try { document.body.style.MozUserSelect = "none"; } catch(e) {}
			try { document.body.style.cursor = "nwse-resize"; } catch(e) {}
		},
		resizeWindowMove: function(e)
		{
			var windowScroll = BX.GetWindowScrollPos();
			var x = e.clientX + windowScroll.scrollLeft;
			var y = e.clientY + windowScroll.scrollTop;

			BX.CrmProductSearchDialogWindow.size.height = this.height = Math.max(y-this.pos.top, this._settings.minHeight);
			BX.CrmProductSearchDialogWindow.size.width = this.width = Math.max(x-this.pos.left, this._settings.minWidth);

			this.contentContainer.style.height = this.height+'px';
			this.contentContainer.style.width = this.width+'px';
		},
		resizeWindowStop: function(e)
		{
			if(document.body.releaseCapture)
				document.body.releaseCapture();

			BX.unbind(document, "mousemove", BX.proxy(this.resizeWindowMove, this));
			BX.unbind(document, "mouseup", BX.proxy(this.resizeWindowStop, this));

			try { document.onmousedown = null; } catch(e) {}
			try { document.body.onselectstart = null; } catch(e) {}
			try { document.body.ondragstart = null; } catch(e) {}
			try { document.body.style.MozUserSelect = ""; } catch(e) {}
			try { document.body.style.cursor = "auto"; } catch(e) {}
		}
	};

	BX.CrmProductSearchDialogWindow.create = function(settings)
	{
		var self = new BX.CrmProductSearchDialogWindow();
		self.initialize(settings);
		return self;
	};
	BX.CrmProductSearchDialogWindow.loadCSS = function(settings)
	{
		BX.ajax({
			method: "GET",
			dataType: 'html',
			url: settings.content_url,
			data: {},
			skipAuthCheck: true
		});
	};


	BX.CrmProductSearchDialogWindow.size = {width: 0, height: 0};
}

if (typeof(BX.Crm.PageEventsManagerClass) === "undefined")
{
	BX.Crm.PageEventsManagerClass = function()
	{
		this._settings = {};
	};

	BX.Crm.PageEventsManagerClass.prototype = {
		initialize: function (settings)
		{
			this._settings = settings ? settings : {};
			this.eventHandlers = {};
		},
		registerEventHandler: function(eventName, eventHandler)
		{
			if (!this.eventHandlers[eventName])
				this.eventHandlers[eventName] = [];
			this.eventHandlers[eventName].push(eventHandler);
			BX.addCustomEvent(this, eventName, eventHandler);
		},
		fireEvent: function(eventName, eventParams)
		{
			BX.onCustomEvent(this, eventName, eventParams);
		},
		unregisterEventHandlers: function(eventName)
		{
			if (this.eventHandlers[eventName])
			{
				for (var i = 0; i < this.eventHandlers[eventName].length; i++)
				{
					BX.removeCustomEvent(this, eventName, this.eventHandlers[eventName][i]);
					delete this.eventHandlers[eventName][i];
				}
			}
		}
	};

	BX.Crm.PageEventsManagerClass.create = function(settings)
	{
		var self = new BX.Crm.PageEventsManagerClass();
		self.initialize(settings);
		return self;
	};
}

// currency format
// https://currency.js.org/

(function(d,c){"object"===typeof exports&&"undefined"!==typeof module?module.exports=c():"function"===typeof define&&define.amd?define(c):(d=d||self,d.currency=c())})(this,function(){function d(b,a){if(!(this instanceof d))return new d(b,a);a=Object.assign({},m,a);var f=Math.pow(10,a.precision);this.intValue=b=c(b,a);this.value=b/f;a.increment=a.increment||1/f;a.groups=a.useVedic?n:p;this.s=a;this.p=f}function c(b,a){var f=2<arguments.length&&void 0!==arguments[2]?arguments[2]:!0,c=a.decimal,g=a.errorOnInvalid;
var e=Math.pow(10,a.precision);var h="number"===typeof b;if(h||b instanceof d)e*=h?b:b.value;else if("string"===typeof b)g=new RegExp("[^-\\d"+c+"]","g"),c=new RegExp("\\"+c,"g"),e=(e*=b.replace(/\((.*)\)/,"-$1").replace(g,"").replace(c,"."))||0;else{if(g)throw Error("Invalid Input");e=0}e=e.toFixed(4);return f?Math.round(e):e}var m={symbol:"$",separator:",",decimal:".",formatWithSymbol:!1,errorOnInvalid:!1,precision:2,pattern:"!#",negativePattern:"-!#"},p=/(\d)(?=(\d{3})+\b)/g,n=/(\d)(?=(\d\d)+\d\b)/g;
d.prototype={add:function(b){var a=this.s,f=this.p;return d((this.intValue+c(b,a))/f,a)},subtract:function(b){var a=this.s,f=this.p;return d((this.intValue-c(b,a))/f,a)},multiply:function(b){var a=this.s;return d(this.intValue*b/Math.pow(10,a.precision),a)},divide:function(b){var a=this.s;return d(this.intValue/c(b,a,!1),a)},distribute:function(b){for(var a=this.intValue,f=this.p,c=this.s,g=[],e=Math[0<=a?"floor":"ceil"](a/b),h=Math.abs(a-e*b);0!==b;b--){var k=d(e/f,c);0<h--&&(k=0<=a?k.add(1/f):k.subtract(1/
f));g.push(k)}return g},dollars:function(){return~~this.value},cents:function(){return~~(this.intValue%this.p)},format:function(b){var a=this.s,c=a.pattern,d=a.negativePattern,g=a.formatWithSymbol,e=a.symbol,h=a.separator,k=a.decimal;a=a.groups;var l=(this+"").replace(/^-/,"").split("."),m=l[0];l=l[1];"undefined"===typeof b&&(b=g);return(0<=this.value?c:d).replace("!",b?e:"").replace("#","".concat(m.replace(a,"$1"+h)).concat(l?k+l:""))},toString:function(){var b=this.s,a=b.increment;return(Math.round(this.intValue/
this.p/a)*a).toFixed(b.precision)},toJSON:function(){return this.value}};return d});