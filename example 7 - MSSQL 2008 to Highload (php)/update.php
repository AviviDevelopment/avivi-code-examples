#!/opt/php72/bin/php
<?php

$_SERVER["DOCUMENT_ROOT"] = realpath(__DIR__ . "/../../");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
@set_time_limit(0);
register_shutdown_function('CatalogUpdate::checkFinishImport');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

class CatalogUpdate
{
    private $MS_SQL;
    private $hlId = 8;

    public static $pathToLog = "log.txt"; //лог файл
    private $daysCutLog = 7; //количество дней хранения информации в логе

    private $host = "127.0.0.1"; //адрес сервера MsSql
    private $port = "1433"; //порт
    private $dbname = "site"; //название db
    private $user = "sit"; //имя пользователя
    private $pass = "123-2020"; // пароль

    private $pageSize = 500; //обработка записей за один шаг

    private $lastElement;
    private $count;
    public static $importIsFinish = false;

    function __construct()
    {
        global $argv;

        try {
            $this->MS_SQL = new PDO("sqlsrv:Server=$this->host,$this->port;Database=$this->dbname", $this->user, $this->pass);
            $this->MS_SQL->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } 
        catch (PDOException $e) 
        {
            self::writeToLog("Error!: " . $e->getMessage());
            die();
        }

        if (empty($argv[1])) 
        {
            self::writeToLog("**** Start update ****");
            $this->lastElement = $this->pageSize;
            $this->count = $this->getCount();
            self::writeToLog("Total: " . $this->count);
        } 
        else 
        {
            $this->lastElement = $argv[1];
            $this->count = $argv[2];
        }
    }

    public static function writeToLog($str)
    {
        $currentFile = fopen(self::$pathToLog, "ab");
        if (!$currentFile)
            throw new Exception("Ошибка записи в лог файл " . self::pathToLog . "\n");
        fwrite($currentFile, date('d.m.Y H:i:s') . " " . $str . "\n");
        fclose($currentFile);
    }

    public function cutLog()
    {
        $file = new SplFileObject(self::pathToLog);
        $file->setFlags(SplFileObject::SKIP_EMPTY);
        $str = '';
        $date = date("d.m.Y", mktime(0, 0, 0, date('m'), date('d') - $this->daysCutLog, date('Y')));
        while (!$file->eof()) 
        {
            $line = $file->fgets();
            $currentDate = strtotime(substr(trim($line), 0, 10));
            if ($currentDate != false && $currentDate > strtotime($date)) $str .= $line;
        }
        $file = null;
        file_put_contents(self::pathToLog, $str);
    }

    public function getProduct($lastElement, $pageSize)
    {
        $result = array();
        if (!empty($this->MS_SQL)) 
        {
            $sql = "WITH num_row
                    AS
                    (
                    SELECT row_number() OVER (ORDER BY ART_ID) as nom , *
                    FROM SITE_GOODS
                    )
                    SELECT * FROM num_row
                    WHERE nom BETWEEN ($lastElement - $pageSize) AND $lastElement";
            foreach ($this->MS_SQL->query($sql, PDO::FETCH_ASSOC) as $row) 
            {
                if (!empty($row['ARTICLE'])) 
                {
                    $result[$row['ART_ID']] = $row;
                }
            }
        }
        return $result;
    }

    public function getCount()
    {
        $result = 0;
        if (!empty($this->MS_SQL)) 
        {
            $stmt = $this->MS_SQL->query('SELECT COUNT(*) FROM SITE_GOODS');
            if ($row = $stmt->fetch()) 
            {
                $result = current($row);
            }
        }
        return $result;
    }

    private function saveItem($arItem)
    {
        $result = true;
        if (!empty($arItem["ARTICLE"])) 
        {
            $ART_ID = (int)$arItem["ART_ID"];
            $tableItem = CHighData::IsRecordExist($this->hlId, array("UF_ART_ID" => $ART_ID), true);
            $arParams = array(
                "UF_ART_ID" => $ART_ID,
                "UF_PRICE" => (float)$arItem["PRICE"],
                "UF_QNT_SITE" => (float)$arItem["QNT_SITE"],
                "UF_ARTICLE" => trim(strtoupper($arItem["ARTICLE"])),
                "UF_SIZE" => trim($arItem["SIZE"]),
                "UF_SIZE2" => trim($arItem["SIZE2"]),
                "UF_HEIGHT" => trim($arItem["HEIGHT"]),
                "UF_HEIGHT2" => trim($arItem["HEIGHT2"])
            );
            if (empty($tableItem)) 
            {
                $result = CHighData::AddRecord($this->hlId, $arParams);
            } 
            else 
            {
                if ($arItem["PRICE"] != $tableItem["UF_PRICE"] ||
                    $arItem["QNT_SITE"] != $tableItem["UF_QNT_SITE"] ||
                    $arItem["SIZE"] != $tableItem["UF_SIZE"] ||
                    $arItem["SIZE2"] != $tableItem["UF_SIZE2"] ||
                    $arItem["HEIGHT"] != $tableItem["UF_HEIGHT"] ||
                    $arItem["HEIGHT2"] != $tableItem["UF_HEIGHT2"]
                ) 
                {
                    $result = CHighData::UpdateRecord($this->hlId, $tableItem["ID"], $arParams);
                }
            }
        }
        return $result;
    }

    public static function checkFinishImport()
    {
        if (!self::$importIsFinish) 
        {
            self::writeToLog("Import has not finished.");
        }
    }

    public function run()
    {
        if (file_exists(__DIR__ . "/stop.txt")) 
        {
            self::writeToLog("Break loading products");
            die();
        }

        $arProduct = $this->getProduct($this->lastElement, $this->pageSize);

        if (empty($arProduct)) 
        {
            self::writeToLog("End loading");
            self::$importIsFinish = true;
            die();
        }

        foreach ($arProduct as $arItem) 
        {
            $res = $this->saveItem($arItem);
            if (!$res) 
            {
                self::writeToLog("Error save item, ART_ID:" . $arItem["ART_ID"]);
            }
        }
        if ($this->lastElement > $this->count) 
        {
            $lastElemNext = $this->count;
        } 
        else 
        {
            $lastElemNext = $this->lastElement + $this->pageSize;
        }
        self::writeToLog("Elements: " . $this->lastElement);

        self::$importIsFinish = true;
        shell_exec("./update.php $lastElemNext $this->count > /dev/null &");
        die();
    }
}

$catalog = new CatalogUpdate();
$catalog->run();