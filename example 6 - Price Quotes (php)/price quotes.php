<? require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->RestartBuffer();
use Bitrix\Main\Loader;

$response = '';

if(!empty($_REQUEST['id'])){

	if (!CModule::IncludeModule('iblock')){
		return;
	}
	if (!CModule::IncludeModule('highloadblock')){
		return;
	}

	$ID = intval($_REQUEST['id']);


	$cache = new CPHPCache();
	$cache_time = 3600*3;
	$cache_id = "rate_{$ID}";
	$cache_path = 'Rates';
	/**/
	if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
	{
		$cachedData = $cache->GetVars();
		$response = $cachedData['response'];
	}
	else
	{

		global $CACHE_MANAGER;
		$CACHE_MANAGER->StartTagCache($cachePath);

		$CACHE_MANAGER->RegisterTag('rates_id_' . $ID);


		$hldata = \Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => ['NAME' => 'Rate']])->fetch();
		$hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
		$hlDataClass = $hldata['NAME'].'Table';

		// получаем данные из Highloadblock
		function hlentity($hlDataClass, $id)
		{
			$result = $hlDataClass::getList(array(
				'select' => array('UF_DATE', 'ID', 'UF_PRICE', 'UF_NET_ASSETS'),
				'order' => array('UF_DATE' => 'ASC'),
				'filter' => array('UF_ELEMENT' => $id),
			));
		 
			$lastMonthDate = new \DateTime();
			$lastMonthDate->setTime(0, 0, 0)->modify('-1 month');

			while($item = $result->fetch()) 
			{
				$dateTime = new \DateTime($item['UF_DATE']->format('d.m.Y 00:00:00'));

				if ($dateTime->getTimestamp() >= $lastMonthDate->getTimestamp()) {
					$item['IN_LAST_MONTH'] = true;
				}

				$item['DATE'] = array(
					'YEAR'  => $item['UF_DATE']->format('Y'),
					'MONTH' => $item['UF_DATE']->format('m'),
					'DAY'   => $item['UF_DATE']->format('d'),
					'HOUR'  => $item['UF_DATE']->format('h'),
				);
				$item['DATE_TIME'] = $dateTime;
				$item['UF_DATE'] = $item['UF_DATE']->format('d.m.Y');
				$item['ID'] = (int)$item['ID'];

				if($firstPrice == null)
				{
					$item['DIFF'] = 0;
					$firstPrice = $item['UF_PRICE'];
				}
				else
				{
					$prevPrice = last($rates);
					$item['DIFF'] = ($item['UF_PRICE'] / $firstPrice - 1) * 100;
				}

				$item['UF_PRICE'] = (float)$item['UF_PRICE'];
				$item['UF_NET_ASSETS'] = (float)$item['UF_NET_ASSETS'];
				$rates[$item['UF_DATE']] = $item;
			}
			return $rates;
		}

		function pickPrevCurrency($date=false, $currency='USD', $rates) 
		{
	        if(!empty($date) && !empty($rates) && $currency=='USD') 
	        {
	            if(!empty($rates[$date])) 
	            {
	                return $rates[$date];
	            } 
	            else 
	            {
	                $start = new DateTime($date);
	                $end = new DateTime($date);
	                $end->modify('- 3 week');
	                $result = '';
	                for($start; $start>=$end; $start->modify('- 1 day')) 
	                {
	                    if(!empty($rates[$start->Format('d.m.Y')])) 
	                    {
	                        $result = $rates[$start->Format('d.m.Y')];
	                        break;
	                    }
	                }
	                if(empty($result)) 
	                {
	                    $result = $rates[$date];
	                }
	                return $result;
	            }
	        } 
	        else 
	        {
	            return $rates[$date];
	        }
	    }

		// Конвертирование в USD
		function convertRates($data, $currencyRates, $usd=false) 
		{
	        $firstPrice = array_get(current($data), 'UF_PRICE');

	        return array_map(function ($item) use ($currencyRates, $firstPrice, $firstPriceUsd) 
	        {
				$firstPriceUsd = null;

	            $item['UF_PRICE_USD'] = 0;

	            foreach ($currencyRates as $currency => $rates) 
	            {
	                $usdRate = pickPrevCurrency($item['UF_DATE'], $currency, $rates);
	                if (isset($usdRate)) 
	                {
						$item['USD_RATE'] = $usdRate;
	                    $item['UF_PRICE_' . $currency] = $item['UF_PRICE'] / $usdRate;
	                    if ($currency == 'USD' && is_null($firstPriceUsd) && $usd) 
	                    {
	                        $firstPriceUsd = $item['UF_PRICE_USD'];
	                    }
	                }
	            }

	            $item['DIFF']     = ($item['UF_PRICE'] / $firstPrice - 1) * 100;
				if($usd)
				{
					$item['DIFF_USD'] = ! is_null($firstPriceUsd) ? ($item['UF_PRICE_USD'] / $firstPriceUsd - 1) * 100 : 0;
				}

	            return $item;

	        }, $data);
	    }

		// Выбираем данные для инфоблока
		$arFilter = array(
			'ID' => $ID
		);
		$arSelect = array(
			'NAME',
			'CODE',
			'PROPERTY_BENCHMARK',
			'PROPERTY_USD',
		);
		
		$block = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect)->Fetch();
		$block['PROPERTY_BENCHMARK_VALUE'] = unserialize($block['PROPERTY_BENCHMARK_VALUE']);
		$block['CODE'] = '/individuals/fund/'.$block['CODE'].'/';
		
		// Если задан вывод USD
		$usd = ($block['PROPERTY_USD_VALUE'] == 'Y') ? true : false;

		if($usd)
		{
			if (!Loader::includeModule('currency')) 
			{
				return;
			}

			// Получаем последнии данные USD
			$dbRates = CCurrencyRates::GetList(($b = "DATE_RATE"), ($o = "desc"), ['CURRENCY' => $codes]);
			$currencyRates = [];
			while ($currency = $dbRates->Fetch()) 
			{
				$currencyRates[$currency['CURRENCY']][$currency['DATE_RATE']] = $currency['RATE'];
			}
		}

		if(!empty($block['PROPERTY_BENCHMARK_VALUE']['VALUE']))
		{
			$benchmarkName = '';
			$resBenchmarkName = CIBlockElement::GetByID($block['PROPERTY_BENCHMARK_VALUE']['VALUE'][0]);
			if($arRes = $resBenchmarkName->Fetch())
			{
				$benchmarkName = $arRes['NAME'];
			}

			$benchmark = hlentity($hlDataClass, $block['PROPERTY_BENCHMARK_VALUE']['VALUE'][0]);
			$benchmark = convertRates($benchmark, $currencyRates, $usd);
		}
		else
		{
			$block['PROPERTY_BENCHMARK_VALUE'] = false;
		}

		$rate = hlentity($hlDataClass, $ID);
		$rate = convertRates($rate, $currencyRates, $usd);

		foreach($rate as $item)
		{
			$arItems = array(
				'date'  => $item['DATE'],
				'dateFormat'  => $item['UF_DATE'],
				'price' => $item['UF_PRICE'],	
				'diff'  => is_nan($rate['DIFF'])|| is_infinite($rate['DIFF']) ? 0 : round($rate['DIFF'], 2),	
				'priceFormatted' => number_format($item['UF_PRICE'], 2, '.', ' '),
				'net_assets' => number_format($item['UF_NET_ASSETS'], 2, '.', ' '),
				'net_assets_raw' => $item['UF_NET_ASSETS'],
				'rate_usd' => $item['USD_RATE']
			);

			if($usd)
			{
				$arItems['price_usd'] = $item['UF_PRICE_USD'];
							$arItems['diff_usd'] =  is_nan($item['DIFF_USD'])|| is_infinite($item['DIFF_USD']) ? 0 : round($item['DIFF_USD'], 2);
				$arItems['priceFormatted_usd'] = number_format($item['UF_PRICE_USD'], 2, '.', ' ');
			}
			
			if (isset($benchmark[$item['UF_DATE']]))
			{
				$arItems['b_price'] = $benchmark[$item['UF_DATE']]['UF_PRICE'];
				$arItems['b_diff'] = round($benchmark[$item['UF_DATE']]['DIFF'], 2);
				if($usd){
					$arItems['b_diff_usd'] = round($benchmark[$item['UF_DATE']]['DIFF_USD'], 2);
					$arItems['b_price_usd'] = $benchmark[$item['UF_DATE']]['UF_PRICE_USD'];
				}
			}
			else
			{
				$arItems['b_price'] = $ratesData[count($ratesData)-1]['b_price'];
				$arItems['b_diff'] = $ratesData[count($ratesData)-1]['b_diff'];
				if($usd)
				{
					$arItems['b_price_usd'] = $ratesData[count($ratesData)-1]['b_price_usd'];
					$arItems['b_diff_usd'] = $ratesData[count($ratesData)-1]['b_diff_usd'];
				}
			}

			$ratesData[] = $arItems;
		}
	  
		$arr = array($ratesData, $block, $benchmarkName);
		$response = json_encode($arr);

		$CACHE_MANAGER->EndTagCache();
		if ($cache_time > 0)
		{
			$cache->StartDataCache($cache_time, $cache_id, $cache_path);
			$cache->EndDataCache(array("response"=>$response));
		}
	}
}

echo $response;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');
?>