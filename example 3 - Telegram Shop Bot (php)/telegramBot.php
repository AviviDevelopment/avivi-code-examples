<?php
require_once (__DIR__.'/lang.php');

class TelegramBot
{
    private function exec_curl_request($handle)
    {
        $response = curl_exec($handle);

        if ($response === false)
        {
            $errno = curl_errno($handle);
            $error = curl_error($handle);
            error_log(CURL_RETURN . $errno . ": $error\n");
            curl_close($handle);
            return false;
        }

        $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
        curl_close($handle);

        if ($http_code >= 500)
        {
            // do not wat to DDOS server if something goes wrong
            sleep(10);
            return false;
        }
        else if ($http_code != 200)
        {
            $response = json_decode($response, true);
            error_log(REQUEST_HAS_FAILED . "{$response['error_code']}: {$response['description']}\n");
            if ($http_code == 401)
            {
                throw new Exception(INVALID_TOKEN);
            }
            return false;
        }
        else
        {
            $response = json_decode($response, true);
            if (isset($response['description']))
            {
                error_log(REQUEST_HAS_SUCCESSFUL . "{$response['description']}\n");
            }
            $response = $response['result'];
        }

        return $response;
    }

    private function apiRequestJson($method, $parameters, $record_last_message = true)
    {
        if (!is_string($method))
        {
            error_log(MUST_BE_STRING_METHOD);
            return false;
        }

        if (!$parameters)
        {
            $parameters = array();
        }
        else if (!is_array($parameters))
        {
            error_log(PARAMS_MUST_BE_AN_ARRAY);
            return false;
        }

        if($method == "sendMessage" && $record_last_message)
        {
            $db = new SafeMySQL();
            $last_message = $db->getRow('SELECT MESSAGE FROM last_message WHERE CHAT_ID = ?i',$parameters['chat_id']);
            if(!empty($last_message))
            {
                $sql = "UPDATE last_message SET MESSAGE=?s WHERE CHAT_ID = ?i";
            }
            else
            {
                $sql = "INSERT INTO last_message (MESSAGE,CHAT_ID) VALUES (?s,?i)";
            }
            $db->query($sql, $parameters['text'], $parameters['chat_id']);
        }

        $parameters["method"] = $method;

        $handle = curl_init(API_URL);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($handle, CURLOPT_TIMEOUT, 60);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
        curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

        return static::exec_curl_request($handle);
    }

    public function processMessage($message)
    {
        $message_id = $message['message_id'];
        $chat_id = $message['chat']['id'];

        $db = new SafeMySQL();
        $res = $db->getRow('SELECT MESSAGE FROM last_message WHERE CHAT_ID = ?i',$chat_id);
        $last_message = $res['MESSAGE'];
        if (isset($message['text']))
        {
            // incoming text message
            $text = $message['text'];

            if (strpos($text, "/start") === 0 || strpos($text, MAIN) === 0)
            {
                //Avivi: if it start
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => HELLO, 'reply_markup' => array(
                    'keyboard' => array(array(CATALOG, BASKET), array(MY_ORDERS)),
                    'resize_keyboard' => true)));
            }
            else if (strpos($text, "/addprod_") === 0 )
            {
                //Avivi: add product to basket
                $this->addProductsToBasket($text, $chat_id, $db, $message_id);
            }
            else if ($text == BASKET_EDIT_CANCEL)
            {
                //Avivi: cancel change products count
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => BASKET_EDIT_WAS_CANCELED, 'reply_markup' => array(
                    'keyboard' => array(array(CHECKOUT, MAIN)),
                )));
            }
            else if ($last_message == BASKET_EDIT_CHANGE)
            {
                //Avivi: change products count
                $this->changeBasketCount ($text, $chat_id, $db);
            }
            else if (strpos($text, "/showmore_") === 0)
            {
                //Avivi: if clicked show more products
                $this->showMoreProducts($text, $chat_id, $db);
            }
            else if (strpos($text, "/editprod_") === 0)
            {
                //Avivi: if clicked change count
                $this->onChangeButtonClicked($text, $chat_id, $db);
            }
            else if (strpos($text, "/deleteprod_") === 0)
            {
                //Avivi: if clicked delete product from basket
                $this->onDeleteButtonClicked($text, $chat_id, $db, $message_id);
            }
            else if ($text === MY_ORDERS)
            {
                //Avivi: show client orders
                $this->showOrders($chat_id);
            }
            else if ($text === BASKET)
            {
                //Avivi: show basket products
                $this->showBasket($db, $chat_id);
            }
            else if ($text === CHECKOUT || $text === USE_DATA || $text === NOT_USE_DATA || $last_message == ENTER_YOUR_FIRST_NAME || $last_message == ENTER_YOUR_LAST_NAME || $last_message == ENTER_YOUR_PHONE || $last_message == ENTER_YOUR_ADDRESS)
            {
                //Avivi: show basket products
                $this->checkoutProcess($text, $chat_id, $db, $last_message);
            }
            else if ($text === CATALOG || $text === CATEGORIES || $last_message == CATALOG_LIST || $last_message == PRODUCTS_LIST)
            {
                //Avivi: if it catalog or category
                $this->showCatalog($text, $last_message, $chat_id, $db);
            }
            else
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => WRONG_COMMAND), false);
            }
        }
        else
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => UNDERSTAND_ONLY_TEXT), false);
        }
    }

    public function setWebHook()
    {
        $res = apiRequestJson('setWebhook', array('url' => WEBHOOK_URL));
        print_r($res);
        exit;
    }

    private function addProductsToBasket($text, $chat_id, $db, $message_id)
    {
        $product_id = explode("_",$text);

        $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
        if (!empty($res))
        {
            $products = json_decode($res['PRODUCTS'],true);
            if (array_key_exists($product_id[1], $products))
            {
                return;
            }
            else
            {
                $sql = "UPDATE basket SET PRODUCTS=?s WHERE CHAT_ID = ?i";
                $products[$product_id[1]] = 1;
                $res = $db->query($sql, json_encode($products), $chat_id);
                $this->apiRequestJson("editMessageReplyMarkup", array(
                    'chat_id' => $chat_id,
                    'message_id' => $message_id,
                    'reply_markup' => array(
                        'inline_keyboard' => array(array(array("text" => WAS_ADD_TO_BASKET, "callback_data" => $text))),
                        'resize_keyboard' => true
                    )
                ));
            }
        }
        else
        {
            $sql = "INSERT INTO basket (PRODUCTS,CHAT_ID) VALUES (?s,?i)";
            $basket[$product_id[1]] = 1;
            $res = $db->query($sql, json_encode($basket), $chat_id);
            $this->apiRequestJson("editMessageReplyMarkup", array(
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'reply_markup' => array(
                    'inline_keyboard' => array(array(array("text" => WAS_ADD_TO_BASKET, "callback_data" => $text))),
                    'resize_keyboard' => true
                )
            ));
        }
    }

    private function changeBasketCount ($text, $chat_id, $db)
    {
        if(!is_numeric($text))
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => NOT_NUMERIC), false);
        }
        else
        {
            if(!is_int(intval($text)))
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => NOT_INTEGER), false);
            }
            else
            {
                $res = $db->getRow('SELECT PRODUCT_ID FROM last_product_change WHERE CHAT_ID = ?i', $chat_id);
                $product_id = $res['PRODUCT_ID'];
                $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
                $products = json_decode($res['PRODUCTS'],true);
                $products[$product_id] = $text;
                $sql = "UPDATE basket SET PRODUCTS=?s WHERE CHAT_ID = ?i";
                $db->query($sql, json_encode($products), $chat_id);
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => BASKET_EDIT_WAS_CHANGED, 'reply_markup' => array(
                    'keyboard' => array(array(CHECKOUT, MAIN)),
                )));
                $this->showBasket($db, $chat_id);
            }
        }
    }

    private function onChangeButtonClicked($text, $chat_id, $db)
    {
        $product_id = explode("_", $text);
        $res = $db->getRow('SELECT PRODUCT_ID FROM last_product_change WHERE CHAT_ID = ?i', $chat_id);
        if (!empty($res))
        {
            $sql = "UPDATE last_product_change SET PRODUCT_ID=?i WHERE CHAT_ID = ?i";
        }
        else
        {
            $sql = "INSERT INTO last_product_change (PRODUCT_ID,CHAT_ID) VALUES (?i,?i)";
        }
        $db->query($sql, $product_id[1], $chat_id);
        $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => BASKET_EDIT_CHANGE, 'reply_markup' => array(
            'keyboard' => array(array(BASKET_EDIT_CANCEL))
        )));
    }

    private function onDeleteButtonClicked($text, $chat_id, $db, $message_id)
    {
        $product_id = explode("_", $text);
        $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
        $products = json_decode($res['PRODUCTS'],true);
        unset($products[$product_id[1]]);
        $sql = "UPDATE basket SET PRODUCTS=?s WHERE CHAT_ID = ?i";
        $db->query($sql, json_encode($products), $chat_id);
        $this->apiRequestJson("deleteMessage", array('chat_id' => $chat_id, 'message_id' => $message_id));
    }

    private function showBasket($db, $chat_id)
    {
        $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
        $basketProducts = json_decode($res['PRODUCTS'],true);
        if(!empty($basketProducts))
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, 'text' => BASKET_PRODUCTS));
            $i = 0;
            do {
                $i++;
                if ($i == 1)
                {
                    $start = 0;
                }
                else
                {
                    $start = $productsRes['next'];
                }

                $arParams = array(
                    'order' => array("NAME" => "ASC"),
                    'filter' => array("ID" => array_keys($basketProducts)),
                    'select' => array(),
                    'start' => $start
                );

                $productsRes = CRest::call('crm.product.list', $arParams);
                foreach ($productsRes['result'] as $product)
                {
                    $products[] = $product;
                }
            }
            while (isset($productsRes['next']));

            //Avivi: get currency format and convertation
            $baseCurrencyRes = CRest::call('crm.currency.base.get');
            $baseCurrency = $baseCurrencyRes['result'];
            $currencyRes = CRest::call('crm.currency.list', array());
            foreach ($currencyRes['result'] as $currency)
            {
                if($currency['CURRENCY'] == $baseCurrency)
                {
                    $currencyFormat = $currency['FORMAT_STRING'];
                }
                else
                {
                    $convertation[$currency['CURRENCY']] = $currency['AMOUNT']/$currency['AMOUNT_CNT'];
                }

            }
            $summ = 0;
            foreach ($products as $product)
            {
                //Avivi: get currency format
                if($product['CURRENCY_ID'] != $baseCurrency)
                {
                    $product['PRICE'] = round($product['PRICE']*$convertation[$product['CURRENCY_ID']]);
                }

                //Avivi: count summ
                $summ = $summ + ($product['PRICE']*$basketProducts[$product['ID']]);

                $price = str_replace("#", $product['PRICE'], $currencyFormat);


                $message = $product['NAME'];
                $params = array(
                    'chat_id' => $chat_id,
                    'photo' => PORTAL_FULL_NAME . $product['DETAIL_PICTURE']['downloadUrl'],
                    'caption' => $message." - ".$price,
                    'reply_markup' => array(
                        'inline_keyboard' => array(array(
                            array("text" => BASKET_EDIT." - ".$basketProducts[$product['ID']], "callback_data" => "/editprod_" . $product['ID']),
                            array("text" => BASKET_DELETE, "callback_data" => "/deleteprod_" . $product['ID'])
                        )),
                        'resize_keyboard' => true
                    )
                );
                if (empty($product['DETAIL_PICTURE']['downloadUrl']))
                {
                    $params['photo'] = WEBHOOK_URL . "noimage.png";
                }
                $this->apiRequestJson("sendPhoto", $params);
            }
            $summ = str_replace("#", $summ, $currencyFormat);
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => SUMM." - ".$summ,'reply_markup' => array(
                'keyboard' => array(array(CHECKOUT, MAIN)),
            )), false);
        }
        else
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => BASKET_IS_EMPTY), false);
        }
    }

    private function checkoutProcess($text, $chat_id, $db, $last_message)
    {
        if($text === CHECKOUT )
        {
            $res = $db->getRow('SELECT * FROM basket WHERE CHAT_ID = ?i', $chat_id);
            $basketProducts = json_decode($res['PRODUCTS'], true);
            if (!empty($basketProducts))
            {
                if(!empty($res['FIRST_NAME']))
                {
                    $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => USE_SAVED_DATA." (".$res['FIRST_NAME']." ".$res['LAST_NAME'].", ".$res['PHONE'].", ".$res['ADDRESS'].")", 'reply_markup' => array(
                        'keyboard' => array(array(USE_DATA, NOT_USE_DATA), array(MAIN)),
                    )));
                }
                else
                {
                    $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => ENTER_YOUR_FIRST_NAME, 'reply_markup' => array(
                        'keyboard' => array(array(MAIN)),
                    )));
                }
            }
            else
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => BASKET_IS_EMPTY), false);
            }
        }

        if(strpos($last_message, USE_SAVED_DATA) === 0 && $text === NOT_USE_DATA)
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => ENTER_YOUR_FIRST_NAME, 'reply_markup' => array(
                'keyboard' => array(array(MAIN)),
            )));
        }

        if($last_message === ENTER_YOUR_FIRST_NAME)
        {
            $sql = "UPDATE basket SET FIRST_NAME=?s WHERE CHAT_ID = ?i";
            $db->query($sql, $text, $chat_id);
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => ENTER_YOUR_LAST_NAME));
        }

        if($last_message === ENTER_YOUR_LAST_NAME)
        {
            $sql = "UPDATE basket SET LAST_NAME=?s WHERE CHAT_ID = ?i";
            $db->query($sql, $text, $chat_id);
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => ENTER_YOUR_PHONE));
        }

        if($last_message === ENTER_YOUR_PHONE)
        {
            $sql = "UPDATE basket SET PHONE=?s WHERE CHAT_ID = ?i";
            $db->query($sql, $text, $chat_id);
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => ENTER_YOUR_ADDRESS));
        }

        if($last_message === ENTER_YOUR_ADDRESS || (strpos($last_message, USE_SAVED_DATA) === 0 && $text === USE_DATA))
        {
            if($text != USE_DATA)
            {
                $sql = "UPDATE basket SET ADDRESS=?s WHERE CHAT_ID = ?i";
                $db->query($sql, $text, $chat_id);
            }
            $basket = $db->getRow("SELECT * FROM basket WHERE CHAT_ID = ?i", $chat_id);

            $params = array(
                'order' => array(),
                'filter' => array(CONTACT_CHAT_ID => $chat_id),
                'select' => array('ID')
            );
            $contact = CRest::call('crm.contact.list', $params);
            if(!empty($contact['result'][0]['ID']))
            {
                $params = array(
                    'id' => $contact['result'][0]['ID'],
                    'fields' => array(
                        "NAME" => $basket['FIRST_NAME'],
                        "LAST_NAME" => $basket['LAST_NAME'],
                        "PHONE" => array(array(
                            "VALUE" => $basket['PHONE'],
                            "VALUE_TYPE" => "WORK"
                        )),
                        CONTACT_ADDRESS => $basket['ADDRESS']
                    ),
                    'params' => array('REGISTER_SONET_EVENT' => 'Y')
                );
                CRest::call('crm.contact.update', $params);
                $contact['result'] = $contact['result'][0]['ID'];
            }
            else
            {
                $params = array(
                    'fields' => array(
                        "NAME" => $basket['FIRST_NAME'],
                        "LAST_NAME" => $basket['LAST_NAME'],
                        "OPENED" => "Y",
                        "TYPE_ID" => "CLIENT",
                        "PHONE" => array(array(
                            "VALUE" => $basket['PHONE'],
                            "VALUE_TYPE" => "WORK"
                        )),
                        CONTACT_ADDRESS => $basket['ADDRESS'],
                        CONTACT_CHAT_ID => $chat_id

                    ),
                    'params' => array('REGISTER_SONET_EVENT' => 'Y')
                );
                $contact = CRest::call('crm.contact.add', $params);
            }

            $basketProducts = json_decode($basket['PRODUCTS'],true);
            $i = 0;
            do
            {
                $i++;
                if ($i == 1)
                {
                    $start = 0;
                }
                else
                {
                    $start = $productsRes['next'];
                }

                $arParams = array(
                    'order' => array("NAME" => "ASC"),
                    'filter' => array("ID" => array_keys($basketProducts)),
                    'select' => array(),
                    'start' => $start
                );

                $productsRes = CRest::call('crm.product.list', $arParams);
                foreach ($productsRes['result'] as $product)
                {
                    $products[] = $product;
                }
            }
            while (isset($productsRes['next']));

            $baseCurrencyRes = CRest::call('crm.currency.base.get');
            $baseCurrency = $baseCurrencyRes['result'];
            $currencyRes = CRest::call('crm.currency.list', array());
            foreach ($currencyRes['result'] as $currency)
            {
                if($currency['CURRENCY'] == $baseCurrency)
                {
                    $currencyFormat = $currency['FORMAT_STRING'];
                }
                else
                {
                    $convertation[$currency['CURRENCY']] = $currency['AMOUNT']/$currency['AMOUNT_CNT'];
                }

            }
            $summ = 0;

            foreach ($products as $product)
            {
                //Avivi: get currency format
                if($product['CURRENCY_ID'] != $baseCurrency)
                {
                    $product['PRICE'] = round($product['PRICE']*$convertation[$product['CURRENCY_ID']]);
                }

                //Avivi: count summ
                $summ = $summ + ($product['PRICE']*$basketProducts[$product['ID']]);

                $rows[] = array(
                    'PRODUCT_ID' => $product['ID'],
                    'PRICE' => $product['PRICE'],
                    'QUANTITY' => $basketProducts[$product['ID']]
                );
            }

            $params = array(
                'fields' => array(
                    "TITLE" => CHECKOUT_DEAL_TITLE.date("d.m.Y H:i:s"),
                    "CONTACT_ID" => $contact['result'],
                    "OPENED" => "Y",
                    "CURRENCY_ID" => $baseCurrency,
                    "OPPORTUNITY" => $summ
                ),
                'params' => array('REGISTER_SONET_EVENT' => 'Y')
            );
            $deal = CRest::call('crm.deal.add', $params);

            $params = array(
                'id' => $deal['result'],
                'rows' => $rows
            );

            CRest::call('crm.deal.productrows.set', $params);

            $sql = "UPDATE basket SET PRODUCTS=?s WHERE CHAT_ID = ?i";
            $db->query($sql, "", $chat_id);

            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => CHECKOUT_IS_DONE, 'reply_markup' => array(
                'keyboard' => array(array(CATALOG, BASKET), array(MY_ORDERS)),
                'resize_keyboard' => true
            )));
        }
    }

    private function showCatalog($text, $last_message, $chat_id, $db)
    {
        $i = 0;
        $sections = array();
        //Avivi: get all categories
        do
        {
            $i++;
            if($i == 1)
            {
                $start = 0;
            }
            else
            {
                $start = $catalogRes['next'];
            }

            $arParams = array(
                'order' => array("NAME" => "ASC"),
                'select' => array(),
                'start' => $start
            );

            //Avivi: if it category
            if(($last_message == CATALOG_LIST || $last_message == PRODUCTS_LIST) && $text != CATALOG)
            {
                $res = $db->getRow('SELECT CATEGORY_ID FROM last_category WHERE CHAT_ID = ?i', $chat_id);
                $last_category = $res['CATEGORY_ID'];
                //Avivi: if we clicked back button
                if($text == BACK)
                {
                    $arParams['filter'] = array("SECTION_ID" => $last_category);
                    $tmp_arParams = array(
                        'order' => array("NAME" => "ASC"),
                        'filter' => array("ID" => $last_category),
                        'select' => array('SECTION_ID')
                    );

                    $tmp_catalogRes = CRest::call('crm.productsection.list', $tmp_arParams);
                    if ($last_category != 0)
                    {
                        $sectionID = ($tmp_catalogRes['result'][0]['SECTION_ID'] == "") ? 0 : $tmp_catalogRes['result'][0]['SECTION_ID'];
                    }
                    else
                    {
                        $sectionID = "";
                    }
                }
                else
                {
                    $tmp_arParams = array(
                        'order' => array("NAME" => "ASC"),
                        'filter' => array("NAME" => $text),
                        'select' => array('ID', 'SECTION_ID')
                    );

                    $tmp_catalogRes = CRest::call('crm.productsection.list', $tmp_arParams);
                    if (!empty($tmp_catalogRes['result']))
                    {
                        $arParams['filter'] = array("SECTION_ID" => $tmp_catalogRes['result'][0]['ID']);
                        $sectionID = ($tmp_catalogRes['result'][0]['SECTION_ID'] == "")? 0 : $tmp_catalogRes['result'][0]['SECTION_ID'];
                    }
                    else
                    {
                        $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => NO_SUCH_CATEGORY), false);
                        return;
                    }
                }
            }
            else
            {
                $arParams['filter'] = array("SECTION_ID" => false);
            }

            $catalogRes = CRest::call('crm.productsection.list', $arParams);
            foreach ($catalogRes['result'] as $catalog)
            {
                $sections[] = $catalog['NAME'];
            }
        }
        while(isset($catalogRes['next']));

        if(!empty($sections))
        {
            //Avivi: record category for back button
            if($text != CATALOG)
            {
                $res = $db->getRow('SELECT CATEGORY_ID FROM last_category WHERE CHAT_ID = ?i', $chat_id);
                if (!empty($res))
                {
                    $sql = "UPDATE last_category SET CATEGORY_ID=?s WHERE CHAT_ID = ?i";
                }
                else
                {
                    $sql = "INSERT INTO last_category (CATEGORY_ID,CHAT_ID) VALUES (?s,?i)";
                }
                $res = $db->query($sql, $sectionID, $chat_id);
            }
            //Avivi: show categories
            if($text !== CATALOG && $sectionID !== "")
            {
                $sections[] = BACK;
            }
            $sections[] = MAIN;
            $i = 0;
            $y = 0;
            foreach ($sections as $section)
            {
                if($y%2 == 0 && $y != 0)
                {
                    $i++;
                    $y = 0;
                }
                $newSections[$i][$y] = $section;
                $y++;
            }
            $message = CATALOG_LIST;
            $params = array('chat_id' => $chat_id, "text" => $message,
                'reply_markup' => array(
                    'keyboard' => $newSections,
                )
            );
            $this->apiRequestJson("sendMessage", $params);
        }
        else
        {
            //Avivi: if there no categories get all products of this category

            $arParams = array(
                'order' => array("NAME" => "ASC"),
                'filter' => array("SECTION_ID" => $tmp_catalogRes['result'][0]['ID']),
                'select' => array(),
                'start' => 0
            );

            $productsRes = CRest::call('crm.product.list', $arParams);
            foreach ($productsRes['result'] as $product)
            {
                $products[] = $product;
            }

            //Avivi: if not empty products, show them
            if(!empty($products))
            {
                //Avivi: record category for back button
                if($text != CATALOG)
                {
                    $res = $db->getRow('SELECT CATEGORY_ID FROM last_category WHERE CHAT_ID = ?i', $chat_id);
                    if (!empty($res))
                    {
                        $sql = "UPDATE last_category SET CATEGORY_ID=?s WHERE CHAT_ID = ?i";
                    }
                    else
                    {
                        $sql = "INSERT INTO last_category (CATEGORY_ID,CHAT_ID) VALUES (?s,?i)";
                    }
                    $res = $db->query($sql, $sectionID, $chat_id);
                }
                //Avivi: get products in basket
                $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
                $basketProducts = json_decode($res['PRODUCTS'],true);
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => PRODUCTS_LIST, 'reply_markup' => array(
                    'remove_keyboard' => true
                )));

                //Avivi: get currency format and convertation
                $baseCurrencyRes = CRest::call('crm.currency.base.get');
                $baseCurrency = $baseCurrencyRes['result'];
                $currencyRes = CRest::call('crm.currency.list', array());
                foreach ($currencyRes['result'] as $currency)
                {
                    if($currency['CURRENCY'] == $baseCurrency)
                    {
                        $currencyFormat = $currency['FORMAT_STRING'];
                    }
                    else
                    {
                        $convertation[$currency['CURRENCY']] = $currency['AMOUNT']/$currency['AMOUNT_CNT'];
                    }

                }

                foreach ($products as $product)
                {
                    if($product['CURRENCY_ID'] != $baseCurrency)
                    {
                        $product['PRICE'] = round($product['PRICE']*$convertation[$product['CURRENCY_ID']]);
                    }

                    //Avivi: set text if product in basket
                    if (array_key_exists($product['ID'], $basketProducts))
                    {
                        $messageText = WAS_ADD_TO_BASKET;
                    }
                    else
                    {
                        $price = str_replace("#", $product['PRICE'], $currencyFormat);
                        $messageText = ADD_TO_BASKET . " – " . $price;
                    }

                    $message = $product['NAME'];
                    $params = array(
                        'chat_id' => $chat_id,
                        'photo' => PORTAL_FULL_NAME . $product['DETAIL_PICTURE']['downloadUrl'],
                        'caption' => $message,
                        'reply_markup' => array(
                            'inline_keyboard' => array(array(array("text" => $messageText, "callback_data" => "/addprod_" . $product['ID']))),
                            'resize_keyboard' => true
                        )
                    );
                    if (empty($product['DETAIL_PICTURE']['downloadUrl']))
                    {
                        $params['photo'] = WEBHOOK_URL . "noimage.png";
                    }
                    $this->apiRequestJson("sendPhoto", $params);
                }
                if(isset($productsRes['next']))
                {
                    $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => "Показано 19 из 50 товаров", 'reply_markup' => array(
                        'inline_keyboard' => array(array(array("text" => MORE_PRODUCTS, "callback_data" => "/showmore_".$tmp_catalogRes['result'][0]['ID']."_".$productsRes['next']))),
                        'resize_keyboard' => true
                    )), false);
                }
                else
                {
                    $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => PRODUCTS_LIST_END, 'reply_markup' => array(
                        'keyboard' => array(array(BACK, MAIN)),
                    )), false);
                }
            }
            else
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => NO_PRODUCTS), false);
            }
        }
    }

    private function showMoreProducts($text, $chat_id, $db)
    {
        $sectionsIDS = explode("_", $text);
        $sectionID = $sectionsIDS[1];
        //Avivi: show more products
        $arParams = array(
            'order' => array("NAME" => "ASC"),
            'filter' => array("SECTION_ID" => $sectionID),
            'select' => array(),
            'start' => $sectionsIDS[2]
        );

        $productsRes = CRest::call('crm.product.list', $arParams);
        foreach ($productsRes['result'] as $product)
        {
            $products[] = $product;
        }

        //Avivi: if not empty products, show them
        if(!empty($products))
        {
            //Avivi: get products in basket
            $res = $db->getRow('SELECT PRODUCTS FROM basket WHERE CHAT_ID = ?i', $chat_id);
            $basketProducts = json_decode($res['PRODUCTS'],true);
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => PRODUCTS_LIST, 'reply_markup' => array(
                'remove_keyboard' => true
            )));

            //Avivi: get currency format and convertation
            $baseCurrencyRes = CRest::call('crm.currency.base.get');
            $baseCurrency = $baseCurrencyRes['result'];
            $currencyRes = CRest::call('crm.currency.list', array());
            foreach ($currencyRes['result'] as $currency)
            {
                if($currency['CURRENCY'] == $baseCurrency)
                {
                    $currencyFormat = $currency['FORMAT_STRING'];
                }
                else
                {
                    $convertation[$currency['CURRENCY']] = $currency['AMOUNT']/$currency['AMOUNT_CNT'];
                }

            }

            foreach ($products as $product)
            {
                if($product['CURRENCY_ID'] != $baseCurrency)
                {
                    $product['PRICE'] = round($product['PRICE']*$convertation[$product['CURRENCY_ID']]);
                }

                //Avivi: set text if product in basket
                if (array_key_exists($product['ID'], $basketProducts))
                {
                    $messageText = WAS_ADD_TO_BASKET;
                }
                else
                {
                    $price = str_replace("#", $product['PRICE'], $currencyFormat);
                    $messageText = ADD_TO_BASKET . " – " . $price;
                }

                $message = $product['NAME'];
                $params = array(
                    'chat_id' => $chat_id,
                    'photo' => PORTAL_FULL_NAME . $product['DETAIL_PICTURE']['downloadUrl'],
                    'caption' => $message,
                    'reply_markup' => array(
                        'inline_keyboard' => array(array(array("text" => $messageText, "callback_data" => "/addprod_" . $product['ID']))),
                        'resize_keyboard' => true
                    )
                );
                if (empty($product['DETAIL_PICTURE']['downloadUrl']))
                {
                    $params['photo'] = WEBHOOK_URL . "noimage.png";
                }
                $this->apiRequestJson("sendPhoto", $params);
            }
            if(isset($productsRes['next']))
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => "Показано 19 из 50 товаров", 'reply_markup' => array(
                    'inline_keyboard' => array(array(array("text" => MORE_PRODUCTS, "callback_data" => "/showmore_".$sectionID."_".$productsRes['next']))),
                    'resize_keyboard' => true
                )), false);
            }
            else
            {
                $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => PRODUCTS_LIST_END, 'reply_markup' => array(
                    'keyboard' => array(array(BACK, MAIN)),
                )), false);
            }
        }
        else
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => NO_PRODUCTS), false);
        }
    }

    public function showOrders($chat_id)
    {
        $arParams = array(
            'order' => array("ID" => "DESC"),
            'filter' => array(CONTACT_CHAT_ID => $chat_id),
            'select' => array("ID", "DATE_CREATE", CONTACT_CHAT_ID),
        );

        $dealsRes = CRest::call('crm.deal.list', $arParams);
        foreach ($dealsRes['result'] as $deal)
        {
            $deals[] = "Order #".$deal['ID']." – ".date('d.m.Y',strtotime($deal['DATE_CREATE']));
        }

        $deals[] = MAIN;

        $i = 0;
        $y = 0;
        foreach ($deals as $deal)
        {
            if($y%2 == 0 && $y != 0)
            {
                $i++;
                $y = 0;
            }
            $newDeals[$i][$y] = $deal;
            $y++;
        }

        if(!empty($deals))
        {
            $params = array('chat_id' => $chat_id, "text" => OREDERS_LIST,
                'reply_markup' => array(
                    'keyboard' => $newDeals,
                )
            );
            $this->apiRequestJson("sendMessage", $params);
        }
        else
        {
            $this->apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => NO_ORDERS), false);
        }
    }
}