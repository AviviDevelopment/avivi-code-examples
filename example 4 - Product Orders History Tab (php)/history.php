<?
AddEventHandler("catalog", "OnBeforeProductUpdate", array("CCustomHistory", "OnProductUpdate"));
AddEventHandler("catalog", "OnBeforePriceUpdate", array("CCustomHistory", "OnBeforePriceUpdate"));
AddEventHandler("main", "OnAdminTabControlBegin", array("CCustomHistory", "OnAdminTabControlBegin"));
AddEventHandler("sale", "OnOrderAdd", array("CCustomHistory", "OnOrderAdd"));

class CCustomHistory
{
	public static function OnOrderAdd($ID, $arFields)
	{
		foreach ($arFields["BASKET_ITEMS"] as $item) 
		{
			self::Add($item["PRODUCT_ID"], "-", "-", "�������� � �����", $ID);
		}
	}

	public static function OnProductUpdate($ID, $arFields)
	{
		if (isset($arFields["QUANTITY"])) 
		{
			$arProduct = CCatalogProduct::GetByID($ID);
			$change = $arFields["QUANTITY"] - $arProduct["QUANTITY"];
			$purchasingPrice = $arProduct['PURCHASING_PRICE'];

			if ($change != 0) 
			{
				self::Add($ID, $change, $arFields["QUANTITY"], "�������");
			}
		}

		if (isset($arFields['PURCHASING_PRICE']))
		{
			$arProduct = CCatalogProduct::GetByID($ID);
			$change = $arFields['PURCHASING_PRICE'] - $arProduct['PURCHASING_PRICE'];

			if ($change != 0)
			{
				self::Add($ID, $change, $arFields['PURCHASING_PRICE'], '���������� ����');
			}
		}
	}

	public static function OnBeforePriceUpdate($ID, $arFields)
	{
		$arPrice = CPrice::GetByID($ID);
		$change = $arFields["PRICE"] - $arPrice["PRICE"];
		if ($change != 0) 
		{
			$prop = "����";
			$arLang = CCatalogGroup::GetLangList(array("CATALOG_GROUP_ID" => $arPrice["CATALOG_GROUP_ID"], "LID" => "ru"));
			if ($arLang = $arLang->Fetch()) {
				$prop = $arLang["NAME"];
			}
			self::Add($arFields["PRODUCT_ID"], $change, $arFields["PRICE"], $prop);
		}
	}

	public static function Add($productID, $change, $value, $prop, $order = false)
	{
		if (\Bitrix\Main\Loader::includeModule('highloadblock')) 
		{
			$arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
			$obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
			$strEntityDataClass = $obEntity->getDataClass();

			$strEntityDataClass::add(array(
				"UF_PRODUCT_ID" => $productID,
				"UF_DATE" => new \Bitrix\Main\Type\DateTime,
				"UF_PROPERTY" => $prop,
				"UF_CHANGE" => $change,
				"UF_VALUE" => $value,
				"UF_USER_ID" => $GLOBALS["USER"]->GetID(),
				"UF_ORDER_ID" => $order
			));
		}
	}

	public static function GetByID($productID)
	{
		static $arUsers = array();
		$arResult = array();

		if (\Bitrix\Main\Loader::includeModule('highloadblock')) 
		{
			$arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
			$obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
			$strEntityDataClass = $obEntity->getDataClass();


			$rsData = $strEntityDataClass::getList(array(
				'filter' => array('UF_PRODUCT_ID' => $productID),
				'order' => array('UF_DATE' => 'DESC')
			));
			while ($arItem = $rsData->Fetch()) 
			{
				$arItem["UF_DATE"] .= '';

				if (!array_key_exists($arItem["UF_USER_ID"], $arUsers)) 
				{
					$arUser = CUser::GetList($by = "", $order = "", array("ID" => $arItem["UF_USER_ID"]), array("SELECT" => array("ID", "LOGIN")));
					if ($arUser = $arUser->Fetch()) {
						$arUsers[$arUser["ID"]] = $arUser;
					}

					$arItem["USER"] = $arUser;
				} 
				else 
				{
					$arItem["USER"] = $arUsers[$arItem["UF_USER_ID"]];
				}

				$arResult[] = $arItem;
			}
		}

		return $arResult;
	}

	public static function GetList($arSort = array('UF_DATE' => 'DESC'), $arFilter, $arNav, $arSelect)
	{
		static $arUsers = array();
		$arResult = array();

		if (\Bitrix\Main\Loader::includeModule('highloadblock')) {
			$arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById(1)->fetch();
			$obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
			$strEntityDataClass = $obEntity->getDataClass();


			$rsData = $strEntityDataClass::getList(array(
				'filter' => $arFilter,
				'order' => $arSort
			));
			while ($arItem = $rsData->Fetch()) 
			{
				$arItem["UF_DATE"] .= '';

				if ($arItem["UF_USER_ID"] > 0) {
					if (!array_key_exists($arItem["UF_USER_ID"], $arUsers)) 
					{
						$arUser = CUser::GetList($by = "", $order = "", array("ID" => $arItem["UF_USER_ID"]), array("SELECT" => array("ID", "LOGIN")));
						if ($arUser = $arUser->Fetch()) 
						{
							$arUsers[$arUser["ID"]] = $arUser;
						}

						$arItem["USER"] = $arUser;
					} 
					else 
					{
						$arItem["USER"] = $arUsers[$arItem["UF_USER_ID"]];
					}
				}

				$arResult[] = $arItem;
			}
		}

		return $arResult;
	}

	public static function OnAdminTabControlBegin(&$oTab)
	{
		if (in_array($GLOBALS["APPLICATION"]->GetCurPage(), array("/bitrix/admin/user_edit.php"))) 
		{
			$userID = $_REQUEST['ID'];

			CModule::includeModule('sale');
			$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), array('USER_ID'=>$userID));
			while ($ar_sales = $db_sales->Fetch())
			{
				$arResult[] = $ar_sales;
			}
			?>
			<?ob_start();?>
			<tr>
				<td>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="internal">
						<tbody>
						<tr class="heading">
							<td>����� ������</td>
							<td>ID ������</td>
							<td>���� ����������</td>
							<td>��������� ������</td>
							<td>�������</td>
						</tr>
						<?foreach ($arResult as $arItem):?>
							<tr>
								<td valign="top" align="center"><a href="/bitrix/admin/sale_order_view.php?ID=<?=$arItem['ID']?>&lang=ru"><?=$arItem["ACCOUNT_NUMBER"]?></a></td>
								<td valign="top" align="center"><a href="/bitrix/admin/sale_order_view.php?ID=<?=$arItem['ID']?>&lang=ru"><?=$arItem["ID"]?></a></td>
								<td valign="top" align="center"><?=$arItem["DATE_INSERT"]?></td>
								<td valign="top" align="right"><?=number_format($arItem['PRICE'], 2, '.', ' ')?></td>
								<td valign="top" align="center"><?=$arItem['CANCELED']=='Y'?'��':'���';?></td>
							</tr>
						<?endforeach;?>
						</tbody>
					</table>
				</td>
			</tr>
			<?
			$content = ob_get_clean();

			$oTab->tabs[] = array(
				"DIV" => "orders",
				"TAB" => "������ ������������",
				"TITLE" => '������� ������� ������������',
				"CONTENT" => $content
			);
		}
		
		if (in_array($GLOBALS["APPLICATION"]->GetCurPage(), array("/bitrix/admin/iblock_element_edit.php", "/bitrix/admin/cat_product_edit.php"))
			&& in_array($_REQUEST["IBLOCK_ID"], array(CATALOG_ID, OFFERS_IBLOCK_ID))
			&& $_REQUEST["ID"] > 0) {
			$productID = intval($_REQUEST["ID"]);

			$arResult = self::GetByID($productID);
			?>
			<?ob_start();?>
			<tr>
				<td>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="internal">
						<tbody>
						<tr class="heading">
							<td>����</td>
							<td>��������</td>
							<td>������ ��������</td>
							<td>���������</td>
							<td>����� ��������</td>
							<td>������������</td>
							<td>�����</td>
						</tr>
						<?foreach ($arResult as $arItem):?>
							<tr>
								<td valign="top"><?=$arItem["UF_DATE"]?></td>
								<td valign="top"><?=$arItem["UF_PROPERTY"]?></td>
								<td valign="top" align="center"><?=$arItem["UF_VALUE"] - $arItem["UF_CHANGE"]?></td>
								<td valign="top" align="center"><?=$arItem["UF_CHANGE"] > 0 ? '+' . $arItem["UF_CHANGE"] : $arItem["UF_CHANGE"]?></td>
								<td valign="top" align="center"><?=$arItem["UF_VALUE"]?></td>
								<td valign="top">
									<?if (isset($arItem["USER"])):?>
									<a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?=$arItem["UF_USER_ID"]?>" target="_blank"><?=$arItem["USER"]["LOGIN"]?>&nbsp;[<?=$arItem["UF_USER_ID"]?>]</a>
									<?endif?>
								</td>
								<td valign="top">
									<?if (isset($arItem["UF_ORDER_ID"])):?>
									<a href="/bitrix/admin/sale_order_view.php?lang=ru&ID=<?=$arItem["UF_ORDER_ID"]?>" target="_blank">[<?=$arItem["UF_ORDER_ID"]?>]</a>
									<?endif?>
								</td>								
							</tr>
						<?endforeach;?>
						</tbody>
					</table>
				</td>
			</tr>
			<?
			$content = ob_get_clean();

			$oTab->tabs[] = array(
				"DIV" => "history",
				"TAB" => "�������",
				"TITLE" => '������� ��������� ��������',
				"CONTENT" => $content
			);
		}

		if (in_array($GLOBALS["APPLICATION"]->GetCurPage(), array("/bitrix/admin/sale_order_view.php")))
		{
			global $APPLICATION;
			$APPLICATION->AddHeadScript('/local/php_interface/lib/admin_scripts.js');
		}
	}
}